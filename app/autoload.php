<?php

use Doctrine\Common\Annotations\AnnotationRegistry;
use Composer\Autoload\ClassLoader;

/**
 * @var ClassLoader $loader
 */
$loader = require __DIR__.'/../vendor/autoload.php';

AnnotationRegistry::registerLoader(array($loader, 'loadClass'));

$loader->add('Knp', __DIR__.'/../vendor/bundles');
$loader->add('Zend\Cache', __DIR__.'/../vendor/zendframework/zend-cache/src');
return $loader;
