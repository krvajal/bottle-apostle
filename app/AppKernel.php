
<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

use BottleApostle\ApiBundle;

class AppKernel extends Kernel
{

    public function init(){

         bcscale(3); // or any other value greater than 0
    }


    public function registerBundles()
    {
        $bundles = array(

            // other bundles
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),

            new Doctrine\Bundle\DoctrineBundle\
            DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new AppBundle\AppBundle(),
            new \FOS\UserBundle\FOSUserBundle(),
            new HWI\Bundle\OAuthBundle\HWIOAuthBundle(),

            new \BottleApostle\UserBundle\BottleApostleUserBundle(),
            new \BottleApostle\RestApiBundle\BottleApostleRestApiBundle(),
            new \BottleApostle\EventsBundle\BottleApostleEventsBundle(),
            new \BottleApostle\EposWebServiceBundle\EposWebServiceBundle(),
            new \BottleApostle\ProductsBundle\BottleApostleProductsBundle(),
            new \BottleApostle\NewsBundle\BottleApostleNewsBundle(),
            
            //sonata bundles
             new Sonata\CoreBundle\SonataCoreBundle(),
            new Sonata\BlockBundle\SonataBlockBundle(),
            new Sonata\EasyExtendsBundle\SonataEasyExtendsBundle(),


            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new Sonata\MediaBundle\SonataMediaBundle(),
            new Application\Sonata\MediaBundle\ApplicationSonataMediaBundle(),
            new Sonata\ClassificationBundle\SonataClassificationBundle(),
            

            //for media bundle
            new Sonata\IntlBundle\SonataIntlBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle(),

            
            new Sonata\DoctrineORMAdminBundle\SonataDoctrineORMAdminBundle(),
            new Sonata\AdminBundle\SonataAdminBundle(),

            new Sonata\UserBundle\SonataUserBundle('FOSUserBundle'),
            new Application\Sonata\UserBundle\ApplicationSonataUserBundle(),
            //ecommerce tools
            new Sonata\NotificationBundle\SonataNotificationBundle(),
            new Sonata\CustomerBundle\SonataCustomerBundle(),
            new Sonata\ProductBundle\SonataProductBundle(),
            new Sonata\BasketBundle\SonataBasketBundle(),
            new Sonata\OrderBundle\SonataOrderBundle(),
            new Sonata\InvoiceBundle\SonataInvoiceBundle(),
            
            new Sonata\DeliveryBundle\SonataDeliveryBundle(),
            new Sonata\PaymentBundle\SonataPaymentBundle(),
            new Sonata\PriceBundle\SonataPriceBundle(),

            // new Sonata\PageBundle\SonataPageBundle(),
            new Sonata\SeoBundle\SonataSeoBundle(),
            new Knp\Bundle\MarkdownBundle\KnpMarkdownBundle(),
            new Ivory\CKEditorBundle\IvoryCKEditorBundle(),
            
            new Sonata\FormatterBundle\SonataFormatterBundle(),
            
            new Sonata\NewsBundle\SonataNewsBundle(),
            //
            new Application\Sonata\CustomerBundle\ApplicationSonataCustomerBundle(),
            new Application\Sonata\DeliveryBundle\ApplicationSonataDeliveryBundle(),
            new Application\Sonata\BasketBundle\ApplicationSonataBasketBundle(),
            new Application\Sonata\InvoiceBundle\ApplicationSonataInvoiceBundle(),
            // new Application\Sonata\MediaBundle\ApplicationSonataMediaBundle(),
            new Application\Sonata\OrderBundle\ApplicationSonataOrderBundle(),
            new Application\Sonata\PaymentBundle\ApplicationSonataPaymentBundle(),
            new Application\Sonata\ProductBundle\ApplicationSonataProductBundle(),
            new Application\Sonata\ClassificationBundle\ApplicationSonataClassificationBundle(),
            new Application\Sonata\NewsBundle\ApplicationSonataNewsBundle(),
            new Nelmio\ApiDocBundle\NelmioApiDocBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),

             new Inori\TwitterAppBundle\InoriTwitterAppBundle(),
             new Oh\InstagramBundle\OhInstagramBundle(),
            //stripe payment
            new WMC\StripeBundle\WMCStripeBundle(),
            
            new FOS\ElasticaBundle\FOSElasticaBundle(),

            new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
            new \Aivus\SonataMediaBundle\ResizerBundle\AivusResizerBundle(),
            new BottleApostle\CuponsBundle\BottleApostleCuponsBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'), true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}
