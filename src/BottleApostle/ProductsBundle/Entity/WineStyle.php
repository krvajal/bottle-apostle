<?php
/**
 * Created by PhpStorm.
 * User: carvajal
 * Date: 9/13/16
 * Time: 6:06 PM
 */

namespace BottleApostle\ProductsBundle\Entity;


use Doctrine\ORM\Mapping as ORM;


/**
 * Class WineStyle
 * @package Application\Sonata\ProductBundle\Entity
 * @ORM\Entity()
 * @ORM\Table("ba_wine_style")
 */
class WineStyle
{


    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected  $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param mixed $color
     */
    public function setColor($color)
    {
        $this->color = $color;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    /**
     * @ORM\Column(type="string",length=200)
     */
    protected $color;
    /**
     * @ORM\Column(type="string",length=200)
     */
    protected $name;

}
