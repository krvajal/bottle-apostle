<?php

namespace BottleApostle\ProductsBundle\Controller;


use Sonata\ProductBundle\Entity\ProductCategoryManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sonata\ClassificationBundle\Entity\CategoryManager;
use Sonata\ClassificationBundle\Model\CategoryInterface;
use Sonata\Component\Currency\CurrencyDetector;
use Sonata\Component\Product\Pool;
use Sonata\ProductBundle\Entity\ProductSetManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;



class CatalogController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/catalog", name="productshome")
     */
    public function indexAction()
    {

        $category = $this->get('sonata.classification.manager.category')->getRootCategory("product_catalog");
        

        return $this->render('default/products/products.html.twig',[
            'root_category' => $category
            ]);
    }

    /**
     * @Route("/catalog/{slug}" , name="categorycatalog")
     */
    public function showCategoryCatalog($slug){

        $category  = $this->getCategoryManager()->findOneBy([
            'slug'    => $slug,
            'enabled' => true]);

        $request = $this->get('request');


        $sortby = $request->get('sortby',1);
        $count = $request->get("count",10); //products per page
        $page = $request->get("page",1);
        $filter = $request->get("filter");
        $pager = $this->get('knp_paginator');


        $option = $this->getRequest()->get('option');

        $sortOptions = ['relevance','lower price','higher price','oldest','newer'];

        $sort = [];
        if($sortby == 1){
            //sort by relevance
            $sort = ["name"=>"ASC"];

        }else if($sortby == 2){
            $sort = ["price"=>"ASC"];
        }else if($sortby == 3){
            $sort = ["price"=>"DESC"];
        }else if($sortby == 4){
            $sort = ["year"=>"ASC"];
        }
        else if($sortby == 5){
            $sort = ["year"=>"DESC"];
        }


        $category_name = $category->getName();

        $template_name = sprintf("default/products/catalog/%s_catalog.html.twig", $category_name);

        if($category){

            $pager = $pager->paginate($this->getProductSetManager()->getCategoryActiveProductsQueryBuilder($category), $page, $count);

           return $this->render($template_name,[
                'pager' => $pager,
               'sortOptions' =>$sortOptions,
               'category' => $category,
               'sortby' => $sortby,
               'currency' => $this->get("sonata.price.currency.detector")->getCurrency()
            ]);
        }else{
//
            throw new NotFoundHttpException("Category not found", 1);
//
        }


    }

    //called from the main product catalog to show 
    // all products for a given category
    /**
     * @param $category
     * @return JsonResponse|Response
     * @Route("/api/v1/products/",name="ba_products_category_provider")
     */
    public function showPreviewCatalogAction( ){


        $request = $this->get("request");
        $category_id = $request->get("category_id");
        $page = $this->getRequest()->get('page', 1);
        $displayMax = $this->getRequest()->get('max', 2);
        $displayMode = $this->getRequest()->get('mode', 'grid');
        $filter = $this->getRequest()->get('filter');
        $option = $this->getRequest()->get('option');

        if (!in_array($displayMode, array('grid'))) { // "list" mode will be added later
            throw new NotFoundHttpException(sprintf('Given display_mode "%s" doesn\'t exist.', $displayMode));
        }

         $category = $this->retrieveCategoryFromId($category_id);


        $this->get('sonata.seo.page')->setTitle($category ? $category->getName() : $this->get('translator')->trans('catalog_index_title'));


        $pager = $this->get('knp_paginator');

        $pagination = $pager->paginate($this->getProductSetManager()->getCategoryActiveProductsQueryBuilder($category, $filter, $option), $page, $displayMax);


        $provider = $this->getProviderFromCategory($category);
        if($provider == null){
            //this means that we dont have any product in this category
            return new Response("No products");

        }


        $providerControllerName = sprintf("%s",$provider->getBaseControllerName()); 

           
                    return $this->render(
                        sprintf('Application%s:small-slider.html.twig',$providerControllerName),
                        [
                            'pager' => $pagination,

                            'category' => $category,
                            'currency' => $this->get("sonata.price.currency.detector")->getCurrency()

                        ]
                    );
       

        
    }


    

    protected function getCategoryManager()
    {
        return $this->get('sonata.classification.manager.category');
    } 

    // /**
    //  * @Route("/{categoryslug}")
    //  */
    // public function showCategoryAction($category){


    // }



    
         /**
     * Retrieves product with id $id or throws an exception if it doesn't exist.
     *
     * @param $id
     *
     * @return ProductInterface
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    protected function getProduct($id)
    {
        $product = $this->getProductSetManager()->findOneBy(array('id' => $id));

        if (null === $product) {
            throw new NotFoundHttpException(sprintf('Product (%d) not found', $id));
        }

        return $product;
    }

     /**
     * @return ProductSetManager
     */
    protected function getProductSetManager()
    {
        return $this->get('sonata.product.set.manager');
    }

     /**
     * @return Pool
     */
    protected function getProductPool()
    {
        return $this->get('sonata.product.pool');
    }

  


    /**
     * Gets the product provider associated with $category if any.
     *
     * @param CategoryInterface $category
     *
     * @return null|\Sonata\Component\Product\ProductProviderInterface
     */
    protected function getProviderFromCategory(CategoryInterface $category = null)
    {
        if (null === $category) {
            return;
        }

        $product = $this->getProductSetManager()->findProductForCategory($category);

        return $product ? $this->getProductPool()->getProvider($product) : null;
    }

     /**
     * Retrieve Category from its id and slug, if any.
     *
     * @return CategoryInterface|null
     */
    protected function retrieveCategoryFromId($categoryId = null) 
    {
    
        if (!$categoryId ) {
            return;
        }
        return $this->getCategoryManager()->findOneBy(array(
            'id' => $categoryId,
            'enabled' => true,
        ));
    }




}
