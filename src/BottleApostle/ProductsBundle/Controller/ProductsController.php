<?php

namespace BottleApostle\ProductsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ProductsController extends Controller
{
    


    public function showNewArrivalsAction()
    {

        
        $products = $this->getProductSetManager()->findAll();
         $currency = $this->get('sonata.basket')->getCurrency();

        return $this->render('components/arrivals-slick.html.twig',[
            'products' => [$products[0]],
            'currency' => $currency
        ]);
    }


    /**
     * @Route("/catalog/{productid}")
     */
    public function showProductAction($productid){

        $product = $this->getProduct($productid);


        $formBuilder = $this->get('form.factory')->createNamedBuilder('add_basket', 'form', null, array('data_class' => $this->container->getParameter('sonata.basket.basket_element.class'), 'csrf_protection' => false));

        // retrieve the custom provider for the product type
        $provider = $this->get('sonata.product.pool')->getProvider($product);

        $provider->defineAddBasketForm($product, $formBuilder);

        $form = $formBuilder->getForm()->createView();
        $currency = $this->get('sonata.basket')->getCurrency();



        return $this->render('default/products/wine_details.html.twig',[
            'product' => $product,
            'form' => $form,
            'provider' => $provider,
            'currency' => $currency
            ]);

    }
    
     
    protected function getProduct($id)
    {
        $product = $this->getProductSetManager()->findOneBy(array('id' => $id));

        if (null === $product) {
            throw new NotFoundHttpException(sprintf('Product (%d) not found', $id));
        }

        return $product;
    }

     /**
     * @return ProductSetManager
     */
    protected function getProductSetManager()
    {
        return $this->get('sonata.product.set.manager');
    }


}
