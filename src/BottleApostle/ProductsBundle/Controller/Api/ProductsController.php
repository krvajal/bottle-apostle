<?php
/**
 * Created by PhpStorm.
 * User: carvajal
 * Date: 10/16/16
 * Time: 9:10 PM
 */



namespace BottleApostle\ProductsBundle\Controller\Api;
use JMS\Serializer\SerializerBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class ProductsController extends  Controller{


    /**
     * @Route("/api/wine_styles")
     */
    public function getStylesAction(){

        $repository  =  $this->getDoctrine()
            ->getRepository('BottleApostleProductsBundle:WineStyle');

        $styles= $repository->findAll();
        $stylesArray = [];
        foreach ($styles as $style){
            $stylesArray[] =  ["color" => $style->getColor(), "name"=> $style->getName(),"id"=>$style->getId()];
        }

        return new JsonResponse($stylesArray);

    }

    /**
     * @Route("/api/wines")
     */
    public function getWinesAction(){

        $request = $this->get("request");
        $count = $request->get("count",10); #products per page
        $page = $request->get("page",0);

        $repository = $this->getDoctrine()->getRepository("ApplicationSonataProductBundle:Wine");
        $products = $repository->findAll();

        $serializer = SerializerBuilder::create()->build();
        $products = $serializer->serialize($products, 'json');
        return new JsonResponse($products);


    }

}
