<?php

/**
 * Created by PhpStorm.
 * User: carvajal
 * Date: 9/13/16
 * Time: 6:18 PM
 */
namespace  BottleApostle\ProductsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin as AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;



class WineStyleAdmin extends  AbstractAdmin{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('color','sonata_type_color_selector')
            ->add('name');


    }
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name');

        $listMapper->add("color");
    }
}