<?php

namespace BottleApostle\RestApiBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sonata\MediaBundle\SonataMediaBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{


    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("search",name="mainsearchendpoint",options={"expose"=true})
     *
     */
    public function searchAction(Request $request)
    {
        $query = $request->get('q');
        $finder = $this->container->get('fos_elastica.finder.app.wine');
        $results = $finder->find("*".$query."*");

//        $query = 'search-string';
//        $mngr = $this->get('fos_elastica.index_manager');
//
//        $search = $mngr->getIndex('app')->createSearch();
//        $search->addType('article');
//        $search->addType('news');
//        $resultSet = $search->search($query);
//
//        $transformer = $this->get('fos_elastica.elastica_to_model_transformer.collection.app');
//        $results = $transformer->transform($resultSet->getResults());
//

        $data = [];
        foreach ( $results as  $wine) {
            $image = $wine->getImage();
            if ($image != null) {

                $provider = $this->container->get($image->getProviderName());
                $format = $provider->getFormatName($image, 'small');
                $url = $provider->generatePublicUrl($image, $format);
            }else{
                $url = "/images/bottle.png";
            }
            $wine_array = [
                "id" => $wine->getId(),
                "slug" => $wine->getSlug(),
                "name" => $wine->getName(),
                "year" => $wine->getYear(),
                "grapes" => $wine->getGrapes(),
                "price" => $wine->getPrice(),
                "type" => $wine->getType(),
                "imageurl" => $url
            ];
            $data [] = $wine_array;
        }

        return new JsonResponse(["results"=>$data]);

    }
}
