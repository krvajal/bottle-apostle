<?php 


namespace BottleApostle\EventsBundle\Controller;


use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use BottleApostle\EventsBundle\Entity\Event;
use BottleApostle\EventsBundle\Entity\EventRepository;


class EventsController extends Controller{


	///return the list of events
	/**
	 * @Route("/",name="eventshome")
	 */
	public function indexAction()
	{
		//get the events from the database
		$events = $event = $this->getDoctrine()
            ->getManager()->getRepository('BottleApostleEventsBundle:Event')->findAll();

		return $this->render('/default/events/list.html.twig',["events"=>$events]);
		
	}

    /**
     * @Route("/show/{id}",name="eventsdetailpage")
     */
	public function showEventAction($id){




        $event = $this->getDoctrine()
            ->getManager()->getRepository('BottleApostleEventsBundle:Event')
            ->find($id);

        if(!$event){
            throw  $this->createNotFoundException("No event found for id ".$id);
        }

        return $this->render('/default/events/details.html.twig',["event"=> $event]);

    }
    function createEvent(){

        $event = new Event();
        $event->setTitle("La marchande");
        $event->setLocation("Location");
        $event->setFromDate(new \DateTime());
        $event->setToDate(new \DateTime());
        $event->setDescription("sample");
        $event->setTickets(10);
        $event->setStatus(0);
        $event->setFree(true);


        $em = $this->getDoctrine()->getManager();
        $em->persist($event);
        $em->flush();
    }

}
 ?>