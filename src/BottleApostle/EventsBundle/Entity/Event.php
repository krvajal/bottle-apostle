<?php 


namespace BottleApostle\EventsBundle\Entity;

use Application\Sonata\CustomerBundle\Entity\Customer;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="BottleApostle\EventsBundle\Entity\EventRepository")
 * @ORM\Table(name="ba_event")
 */
class Event{


	  /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
	private $id;
	  /**
     * @ORM\Column(type="string",length=100)
     */
	private $title;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */

    private $image;



    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Gallery",  cascade={"all"})
     */

    private $gallery;



    /**
     * @ORM\Column(type="date")
     */
	private $fromDate;

    /**
     * @ORM\Column(type="date")
     */
	private $toDate;


    /**
     * @ORM\Column(type="text")
     */
	private $description;

    /**
     * @ORM\Column(type="integer")
     */
	private $tickets;

    /**
     * @ORM\Column(type="integer")
     */
	private $status = 1;
	/**
	 * @ORM\ManyToOne(targetEntity="Application\Sonata\CustomerBundle\Entity\Customer")
	 */
	private $atendees;

    /**
     * @ORM\Column(type = "boolean")
     */
    private $free = false;
    /**
     * @ORM\Column(type = "decimal")
     */
    private $price;
    /**
     * @ORM\Column(type = "string", length=100)
     */
    public $location;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Event
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set fromDate
     *
     * @param \DateTime $fromDate
     * @return Event
     */
    public function setFromDate($fromDate)
    {
        $this->fromDate = $fromDate;

        return $this;
    }

    /**
     * Get fromDate
     *
     * @return \DateTime
     */
    public function getFromDate()
    {
        return $this->fromDate;
    }

    /**
     * Set toDate
     *
     * @param \DateTime $toDate
     * @return Event
     */
    public function setToDate($toDate)
    {
        $this->toDate = $toDate;

        return $this;
    }

    /**
     * Get toDate
     *
     * @return \DateTime
     */
    public function getToDate()
    {
        return $this->toDate;
    }

    /**
     * Set description
     *
     * @param \DateTime $description
     * @return Event
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return \DateTime
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set tickets
     *
     * @param integer $tickets
     * @return Event
     */
    public function setTickets($tickets)
    {
        $this->tickets = $tickets;

        return $this;
    }

    /**
     * Get tickets
     *
     * @return string
     */
    public function getTickets()
    {
        return $this->tickets;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Event
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set atendees
     *
     * @return Event
     */
    public function setAtendees(Customer $atendees = null)
    {
        $this->atendees = $atendees;

        return $this;
    }

    /**
     * Get atendees
     *
     * @return Customer
     */
    public function getAtendees()
    {
        return $this->atendees;
    }

    /**
     * Set imageUrl
     *
     * @param string $imageUrl
     * @return Event
     */
    public function setImageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }

    /**
     * Get imageUrl
     *
     * @return string
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    /**
     * Set free
     *
     * @param boolean $free
     * @return Event
     */
    public function setFree($free)
    {
        $this->free = $free;
        $this->price = 0.0;

        return $this;
    }

    /**
     * Get free
     *
     * @return boolean
     */
    public function isFree()
    {
        return $this->free;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Event
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return Event
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Get free
     *
     * @return boolean 
     */
    public function getFree()
    {
        return $this->free;
    }

    public function setImage( $media)
    {
        $this->image = $media;
    }

    /**
     * @return MediaInterface
     */
    public function getImage()
    {
        return $this->image;
    }
    /**
     * @param MediaInterface $media
     */
    public function setGallery( $gallery)
    {
        $this->gallery = $gallery;
    }

    /**
     * @return MediaInterface
     */
    public function getGallery()
    {
        return $this->gallery;
    }


}
