<?php

namespace BottleApostle\EposWebServiceBundle\Services;

use Symfony\Bridge\Monolog\Logger;

class HelloService
{


    private $logger;

    public function __construct($logger)
    {
        $this->logger = $logger;
    }

    public function Hello($name)
    {

        // $message  =  \SwiftMessage::newInstance()
        //              ->setTo('me@example.com')
        //              ->setSubject('Hello Service');
        // $this->mailer->send($message);
        return "Hello, " . $name;

    }


    public function Update($parameters)
    {

        $this->logger->info("Update Operation starting");
        $this->logger->info($parameters->updateXml);
        $this->logger->info("Hi from updateOrders");
        $this->logger->info("Update Operatin end");
        return array("hello" => "a");

    }


    public function updateOrders($parameters)
    {

        $this->logger->info("Test");
        $this->logger->info($parameters->updateXml);
        $this->logger->info("Hi from updateOrders");
        return array("hello" => "a");
    }


}

