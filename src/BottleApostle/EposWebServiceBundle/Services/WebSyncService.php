<?php

namespace BottleApostle\EposWebServiceBundle\Services;

use Application\Sonata\ProductBundle\Entity\Product;
use Application\Sonata\ProductBundle\Entity\VatRate;
use Application\Sonata\ProductBundle\Entity\Wine;
use Application\Sonata\ProductBundle\Services\ProductsProcessingService;
use BottleApostle\EposWebServiceBundle\Entity\WebSyncTask;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\Validator\Constraints\Date;

/*
 *
 * Operations Supported:
 * Update: Gets products from the epos and update our database
 * UpdateOrders: Send orders to the epos to be processed
 */

class WebSyncService
{


    private $logger;

    private $em;
    private $products_processor;

    private $ok_response_str = "<ns1:UpdateResponse><Status>OK</Status></ns1:UpdateResponse>";
    private $error_response_str = "<ns1:UpdateResponse><Status>Error</Status></ns1:UpdateResponse>";
    private $runningTask = null;


    public function __construct($logger, EntityManager $em, ProductsProcessingService $products_processor)
    {

        $this->logger = $logger;
        $this->em = $em;
        $this->products_processor = $products_processor;
    }

    /*
     * This is one of the operations the webservice support
     * It gets the products from the vector database
     * and maps them to our database
     *
     */
    public function Update($data)
    {

        $valueArray = get_object_vars($data);
        $updateXml =  $valueArray["updateXml"];

//        return new \SoapVar($this->ok_response_str, XSD_ANYXML);

        $xml= simplexml_load_string($updateXml);

        $this->logger->info("Websync request received");

        if($this->isStartRequest($xml)){

            if(!$this->isRunningTask()){
                //no task running, so create a new one

                //create running task

                $this->runningTask = new WebSyncTask($xml->isstart->type);
                $this->runningTask->setStartDate(new \DateTime());
                $this->runningTask->setRunning(true);

                $this->em->persist($this->runningTask);
                $this->em->flush();


                // return ok here
                return new \SoapVar($this->ok_response_str, XSD_ANYXML);

            }else{

                // a new start was requested but a task is running so return Error
                // this should stop the requets from continue

            }
        }
        else if($this->isRunningTask()){
            // a task is running, use it for handling the request

            if(count($xml->products)){

                return $this->products_processor->process($xml->products);
            }else if(count($xml->vats)){

                return $this->processVats($xml->vats);
            }
            else if(count($xml->stockqtys)){

                return $this->processStockQtys($xml->stockqtys);
            }
            else if(count($xml->iscomplete)){
               return  $this->endTask();

            }

        }else{

            //got an order but not start was called before, so refuse it

        }


    }

    /*
     * check is the entry block contains a
     * request to start a new task
     * the xml should be like
        [isstart] => SimpleXMLElement Object
        (
            [start] => true
            [type] => All
        )

     */
    private  function  isStartRequest($xml){

        if(count($xml->isstart) && $xml->isstart->start){

                return true;
        }
        return false;
    }



    /*
     * Check if there is a websync running
     *
     */
    private  function isRunningTask(){

        return $this->getRunningTask() != null;

    }

    /*
     * Query the database for an existing running websync task
     */
    private  function  getRunningTask(){
        $this->logger->info("Getting the websync task from database");
        $repository = $this->em->getRepository("EposWebServiceBundle:WebSyncTask");
        if($this->runningTask == null ){
            $runningTask =  $repository->findOneBy(["running"=>true]);


        }
        $this->logger->info("Got the websync task from database");

        return $runningTask;

    }

    /**
     * @param $parameters
     * @return array
     * This is the other operation the WebSync service supports
     * It sends the order to the system
     */


    public function updateOrders($parameters)
    {

        $this->logger->info("Test");
        $this->logger->info($parameters->updateXml);
        $this->logger->info("Hi from updateOrders");
        return array("hello" => "a");
    }

    /**
     * @param $vats
     * @return \SoapVar
     * This is the inbound xml for this section
      [0] => SimpleXMLElement Object
        (
        [vatid] => 1
        [name] => Zero Rate
        [rate] => 0
        [pharm_vatcode] => SimpleXMLElement Object
        (
        )

        [deleted] => false
        )
         */

        private  function processVats($vats){

            $this->logger->info("Processing vats");
            $vat_repository = $this->em->getRepository("ApplicationSonataProductBundle:VatRate");
            foreach($vats->vat as $vector_vat){
                    $id = $vector_vat->vatid;
                    $vat = $vat_repository->findOneBy(["vector_id"=>$id]);
                    if($vat ==  null){
                        $vat = new VatRate();
                    }
                    $vat->setName($vector_vat->name);
                    $vat->setVectorId($vector_vat->vatid);
                    $vat->setRate(intval($vector_vat->rate));
                    $vat->setDeleted(boolval($vector_vat->deleted));
                    $this->em->persist($vat);
        }
        $this->em->flush();
        return new \SoapVar($this->ok_response_str,XSD_ANYXML);

    }

    /*
     * Updates the stock levels of the products from
     * the vector system
     * the input is an xml array with this elements
     * [74] => SimpleXMLElement Object
                        (
                            [StockID] => 31000000000000272
                            [qty] => 36
                        )
     * where stockID is the Id of the product and qty the stock level
     *
     */

    private function  processStockQtys($stockqtys){

        $wine_repository = $this->em->getRepository("ApplicationSonataProductBundle:Wine");
        foreach ($stockqtys->stockqty as $vector_stock_qty){

            $product_id = $vector_stock_qty->StockID;
            $stock_level = $vector_stock_qty->qty;
            $wine = $wine_repository->findOneBy(["vector_id"=>$product_id]);

            if($wine != null){
                //update stock level and save to database
                $wine->setStock(intval($stock_level));
                $this->em->persist($wine);

            }else{

                //product not found in database
                return new \SoapVar($this->error_response_str,XSD_ANYXML);
            }


        }

        $this->em->flush();

        return new \SoapVar($this->ok_response_str,XSD_ANYXML);
    }

    /**
     * end the websync task
     */
    private function endTask()
    {
        $task = $this->getRunningTask();
        $task->setRunning(false);
        $task->setEndData(new \DateTime());
        $task->setSuccess(true);
        $this->em->persist($task);
        $this->em->flush();
        return new \SoapVar($this->ok_response_str,XSD_ANYXML);
    }


}

