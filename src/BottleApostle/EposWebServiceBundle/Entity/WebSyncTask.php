<?php

namespace BottleApostle\EposWebServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;


/**
 * @ORM\Entity
 * @ORM\Table(name="ba_websync_task")
 */

class WebSyncTask{



    public function __construct($type)
    {

            $this->type =  $type;

    }

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="date")
     */
    protected $startDate;
    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $endData;
    /**
     * @ORM\Column(type="boolean",nullable=true)
     */
    protected $success;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $running = true;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $type;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->startDate;
    }



    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return WebSyncTask
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Set endData
     *
     * @param \DateTime $endData
     *
     * @return WebSyncTask
     */
    public function setEndData($endData)
    {
        $this->endData = $endData;

        return $this;
    }

    /**
     * Get endData
     *
     * @return \DateTime
     */
    public function getEndData()
    {
        return $this->endData;
    }

    /**
     * Set success
     *
     * @param boolean $success
     *
     * @return WebSyncTask
     */
    public function setSuccess($success)
    {
        $this->success = $success;

        return $this;
    }

    /**
     * Get success
     *
     * @return boolean
     */
    public function getSuccess()
    {
        return $this->success;
    }

    /**
     * Set running
     *
     * @param boolean $running
     *
     * @return WebSyncTask
     */
    public function setRunning($running)
    {
        $this->running = $running;
        if($running == false){
            $this->endData = new DateTime();
        }
        return $this;
    }

    /**
     * Get running
     *
     * @return boolean
     */
    public function getRunning()
    {
        return $this->running;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return WebSyncTask
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
