<?php

namespace BottleApostle\CuponsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DefaultController extends Controller
{
    /**
     * @Route("/api/cupons")
     */
    public function getAction()
    {
            $request = $this->get("request");

            $code = $request->get("code","");
            if(strlen($code) > 0){

                $repository = $this->get("doctrine")->getRepository("BottleApostleCuponsBundle:Cupon");
                $cupon = $repository->findOneBy(["code"=>$code]);
                if($cupon == null){
                    return new JsonResponse(["valid"=>false,"error"=>"Not found"]);
                }else{
                    $date = $cupon->getExpiresAt();
                    if($date > new \DateTime()){

                        return new JsonResponse(["valid"=>true,"discount"=>$cupon->getDiscountPercent(),"expiresAt"=>$cupon->getExpiresAt()]);
                    }else{
                        return new JsonResponse(["valid"=>false,"error"=>"Expired"]);

                    }

                }



            }else{

                return new JsonResponse(["valid"=>false,"error"=>"no code received"]);

            }


    }




}
