<?php

namespace BottleApostle\CuponsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cupon
 *
 * @ORM\Table(name="cupon")
 * @ORM\Entity(repositoryClass="BottleApostle\CuponsBundle\Repository\CuponRepository")
 */
class Cupon
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, unique=true)
     */
    private $code;

    /**
     * @var int
     *
     * @ORM\Column(name="discount_percent", type="integer")
     */
    private $discountPercent;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expires_at", type="datetime")
     */
    private $expiresAt;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     *
     * @ORM\OneToMany(targetEntity="BottleApostle\CuponsBundle\Entity\Charge",mappedBy="cupon")
     */

    private $charges;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Cupon
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set discountPercent
     *
     * @param integer $discountPercent
     *
     * @return Cupon
     */
    public function setDiscountPercent($discountPercent)
    {
        $this->discountPercent = $discountPercent;

        return $this;
    }

    /**
     * Get discountPercent
     *
     * @return int
     */
    public function getDiscountPercent()
    {
        return $this->discountPercent;
    }

    /**
     * Set expiresAt
     *
     * @param \DateTime $expiresAt
     *
     * @return Cupon
     */
    public function setExpiresAt($expiresAt)
    {
        $this->expiresAt = $expiresAt;

        return $this;
    }

    /**
     * Get expiresAt
     *
     * @return \DateTime
     */
    public function getExpiresAt()
    {
        return $this->expiresAt;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Cupon
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set charges
     *
     * @param \BottleApostle\CuponsBundle\Entity\Charge $charges
     *
     * @return Cupon
     */
    public function setCharges(\BottleApostle\CuponsBundle\Entity\Charge $charges = null)
    {
        $this->charges = $charges;

        return $this;
    }

    /**
     * Get charges
     *
     * @return \BottleApostle\CuponsBundle\Entity\Charge
     */
    public function getCharges()
    {
        return $this->charges;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->charges = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add charge
     *
     * @param \BottleApostle\CuponsBundle\Entity\Charge $charge
     *
     * @return Cupon
     */
    public function addCharge(\BottleApostle\CuponsBundle\Entity\Charge $charge)
    {
        $this->charges[] = $charge;

        return $this;
    }

    /**
     * Remove charge
     *
     * @param \BottleApostle\CuponsBundle\Entity\Charge $charge
     */
    public function removeCharge(\BottleApostle\CuponsBundle\Entity\Charge $charge)
    {
        $this->charges->removeElement($charge);
    }
}
