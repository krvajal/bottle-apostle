<?php

namespace BottleApostle\CuponsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Charge
 *
 * @ORM\Table(name="charge")
 * @ORM\Entity(repositoryClass="BottleApostle\CuponsBundle\Repository\ChargeRepository")
 */
class Charge
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal", precision=10, scale=0)
     */
    private $amount;

    /**
     * @var \stdClass
     *
     * @ORM\ManyToOne(targetEntity="BottleApostle\CuponsBundle\Entity\Charge", inversedBy="charges")
     */
    private $cupon;

    /**
     * @var string
     *
     * @ORM\Column(name="stripe_id", type="string", length=255, nullable=true, unique=true)
     */
    private $stripeId;

    /**
     * @ORM\Column(type="boolean")
     *
     */
    private $success;

    /**
     * @ORM\Column(type="string")
     */
    private $order_reference;
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return Charge
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set cupon
     *
     * @param \stdClass $cupon
     *
     * @return Charge
     */
    public function setCupon($cupon)
    {
        $this->cupon = $cupon;

        return $this;
    }

    /**
     * Get cupon
     *
     * @return \stdClass
     */
    public function getCupon()
    {
        return $this->cupon;
    }

    /**
     * Set stripeId
     *
     * @param string $stripeId
     *
     * @return Charge
     */
    public function setStripeId($stripeId)
    {
        $this->stripeId = $stripeId;

        return $this;
    }

    /**
     * Get stripeId
     *
     * @return string
     */
    public function getStripeId()
    {
        return $this->stripeId;
    }

    /**
     * Set success
     *
     * @param boolean $success
     *
     * @return Charge
     */
    public function setSuccess($success)
    {
        $this->success = $success;

        return $this;
    }

    /**
     * Get success
     *
     * @return boolean
     */
    public function getSuccess()
    {
        return $this->success;
    }

    /**
     * Set orderReference
     *
     * @param string $orderReference
     *
     * @return Charge
     */
    public function setOrderReference($orderReference)
    {
        $this->order_reference = $orderReference;

        return $this;
    }

    /**
     * Get orderReference
     *
     * @return string
     */
    public function getOrderReference()
    {
        return $this->order_reference;
    }
}
