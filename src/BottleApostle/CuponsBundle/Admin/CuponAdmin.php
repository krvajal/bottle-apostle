<?php


namespace BottleApostle\CuponsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin as AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;



class CuponAdmin extends  AbstractAdmin{


    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('code', 'text',["label"=>"code"])
            ->add("expiresAt","datetime", ["label"=>"Expiration date"])
            ->add("discount_percent","integer",["label"=>"Discount percent"])
            ->add('description','textarea');




    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('code');
        $datagridMapper->add("expiresAt");

    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('code');
        $listMapper->add("expiresAt");
        $listMapper->add("discount_percent");


    }

}