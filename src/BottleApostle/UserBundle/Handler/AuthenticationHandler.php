<?php

namespace BottleApostle\UserBundle\Handler;

use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;

/**
 * Created by PhpStorm.
 * Date: 7/30/2016
 * Time: 12:58 PM
 */

class AuthenticationHandler implements  AuthenticationSuccessHandlerInterface,  AuthenticationFailureHandlerInterface
{
    private $router;
    private $session;
    private $mailer;

    public function __construct(Router   $router, $mailer){

        $this->router = $router;
        $this->mailer = $mailer;

    }
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        if($request->isXmlHttpRequest()){

            $data = array('success'=>false,'message'=>$exception->getMessage());
            $response = new JsonResponse($data);
        }else{
            // Create a flash message with the authentication error message
            $request->getSession()->getFlashBag()->add('error', $exception->getMessage());
            $url = $this->router->generate('fos_user_security_login');

            return new RedirectResponse($url);
        }
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {


        if($request->isXmlHttpRequest()){



            $data = array('success'=>true);
            return new JsonResponse($data);

        }else{


            // redirect him back to that resource
            if ($targetPath = $request->getSession()->get('_security.target_path')) {
                $url = $targetPath;
            } else {
                // Otherwise, redirect him to wherever you want
                $url = $this->router->generate('homepage');
            }

            return new RedirectResponse($url);

        }
    }


}