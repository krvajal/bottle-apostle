<?php

namespace BottleApostle\UserBundle\Controller;

use Application\Sonata\UserBundle\Entity\User;
use BottleApostle\UserBundle\Entity\Customer;
use BottleApostle\UserBundle\Form\RegistrationType;

use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Validator\Constraints\Date;


class RegistrationController extends Controller
{

    /**
     * @param Request $request
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Route("/register", name="user_register")
     */

    public function registerAction(Request $request)
    {


        $this->get("logger")->info("registering");

        $userManager = $this->get('fos_user.user_manager');

        $user = $userManager->createUser();


        $form = $this->createForm(RegistrationType::class, $user);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $password = $this->get('security.password_encoder')->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            $user->setEnabled(true);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            if($request->isXmlHttpRequest()){

                $mailer = $this->get("mailer");


                sendWelcomeEmail($user);


                return new JsonResponse(["success"=> true]);
            }else {



                return $this->redirectToRoute("homepage");
            }

        }else{

            if($request->isXmlHttpRequest()){
                //TODO: add errors here
                return new JsonResponse(['success'=>false]);
            }

            //this should never be rendered
            return $this->render('components/register-form.twig',['form'=> $form->createView()]);
        }


    }


    private  function sendWelcomeEmail(User $user){

        $message_content = $this->render("emails/welcome_template.html.twig",[
            'first_name' => $user->getFirstname(),
            'last_name' => $user->getLastname(),
            'date' => new Date()
        ]);
        $message = Swift_Message::newInstance()
            ->setFrom('welcome@bottleapostle.com')
            ->setTo($user->getEmail())
            ->setSubject('Welcome to Bottle Appostle')
            ->setBody($message_content,"text/html");


        $mailer = $this->get('mailer');
        //Pass a variable name to the send() method
        if (!$mailer->send($message, $failures))
        {
            echo "Failures:";
            var_dump($failures);
            die();
        }


    }




}
