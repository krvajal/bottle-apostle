<?php
/**
 * Created by PhpStorm.
 * User: carvajal
 * Date: 10/20/16
 * Time: 10:57 AM
 */

namespace BottleApostle\NewsBundle\Controller;


use AppBundle\Entity\NewsletterSubscription;
use Sonata\PageBundle\Exception\PageNotFoundException;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


class NewsletterController extends  Controller
{

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/api/newsletter/subscribe",name="newsletter_subcription_api")
     */

    public function subscribeAction(Request $request){

        if($request->isXmlHttpRequest()){
            $name = $request->get('name');
            $email = $request->get('email');
            if($this->isEmailSubscribed($email)){

                return new JsonResponse([
                    "success" => false,
                    "message" => "Already subscribed"
                ]);

            }

            $subscription  = new NewsletterSubscription();
            $subscription->setEmail($email);
            $subscription->setName($name);
            $subscription->setCreatedAt(new \DateTime());
            $em = $this->get('doctrine')->getEntityManager();
            $em->persist($subscription);
            $em->flush();

            $this->sendWelcomeEmail($subscription);


            return new JsonResponse([
                'success'=>true
            ]);


        }else{
            throw new PageNotFoundException();
        }

    }

    /**
     * @param $email
     * @return bool
     * Checks if the email address is already subscribed or not.
     */
    public function  isEmailSubscribed($email){

        $repository  = $this->get('doctrine')->getRepository("AppBundle:NewsletterSubscription");
        $result = $repository->findOneBy(["email"=>$email]);
        if($result == null){
            return false;
        }else{
            return true;
        }

    }
    public function sendWelcomeEmail(NewsletterSubscription $subscription){

        $mailer = $this->get("mailer");
        $message_content = $this->render("emails/newsletter_welcome_template.html.twig",[
            'name'=> $subscription->getName()
        ]);
        $message = Swift_Message::newInstance()
            ->setFrom('no-reply@bottleapostle.com')
            ->setTo($subscription->getEmail())
            ->setSubject('Welcome to your Bottle Apostle Newsletter Subscription')
            ->setBody($message_content,"text/html");


        $mailer = $this->get('mailer');
        //Pass a variable name to the send() method
        if (!$mailer->send($message, $failures))
        {
            echo "Failures:";
            var_dump($failures);
            die();
        }
    }




}