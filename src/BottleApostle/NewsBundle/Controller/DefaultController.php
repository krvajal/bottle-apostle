<?php

namespace BottleApostle\NewsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


class DefaultController extends Controller
{

    /**
     * @Route("/", name="newshome")
     */
    public function indexAction()
    {
        return $this->render('default/news/list.html.twig');
    }
}
