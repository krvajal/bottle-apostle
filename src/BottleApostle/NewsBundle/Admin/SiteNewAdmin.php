<?php

/**
 * Created by PhpStorm.
 * User: paloma
 * Date: 08/08/16
 * Time: 22:53
 */

namespace BottleApostle\NewsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin as AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;



class SiteNewAdmin extends  AbstractAdmin{


    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('title', 'text');

        $formMapper->add('createdAt','datetime');

        $formMapper->add('content','textarea');




    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('title');

    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('title');
        $listMapper->addIdentifier('createdAt','datetime');


    }

}