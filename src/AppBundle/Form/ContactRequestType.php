<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;



class ContactRequestType extends  AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $shops = [];
        if(array_key_exists('shops',$options))
         $shops =  $options['shops'];

        $builder
            ->add('enquiry', ChoiceType::class, array(
                'choices' => array(
                    'Enquiry 1',
                    'Enquiry 2',
                    'Enquiry 3'
                ),
                'placeholder' => 'SELECT'
            ))
            ->add('shop','entity', array(
                'choices' => $shops,
                'class' => 'AppBundle\Entity\Shop',
                'expanded' => false,
                'multiple' => false,
                'mapped' => true,
                'choice_label' => 'name',
                'placeholder' => "SELECT SHOP"
            ))
            ->add('email', EmailType::class)
            ->add('message', TextareaType::class)
            ->add('submit', SubmitType::class, array("label" => "SUBMIT"));


    }



    public function buildView(FormView $view, FormInterface $form, array $options)
    {
//        $view->vars['shops'] = $options['shops'];
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\ContactRequest',
            'shops' => array()
        ));
    }
}