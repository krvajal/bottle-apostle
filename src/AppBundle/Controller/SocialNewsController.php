<?php


namespace AppBundle\Controller;







use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;



class SocialNewsController extends Controller
{
    


    public function showTweetsAction()
    {   


        return $this->render('components/tweets.html.twig');

    }

    /**
     * @Route("/social/instagram")
     */
    public function instagramPostsAction()
    {

        /**
         * @var InstaphpAdapter
         */
        // $api = $this->get('instaphp');
        // $media = $api->Users->Recent('self');

        $images = [];
        // if (empty($media->error)) {
        // foreach ($media->data as $item) {

        //         $images[] = $item['images']['low_resolution']['url'];
        //     }
        // }
                
        //  var_dump($images);

        return $this->render('components/instagram.html.twig',[
            'images' => $images
        ]);

    }

}