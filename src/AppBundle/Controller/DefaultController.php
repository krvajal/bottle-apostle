<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CartItem;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/home/home.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
        ));
    }

    /**
     * @Route("/slides/{no}", name="slidepage")
     */
    public function slide(Request $request, $no)
    {
        if ($no == 1) {

            // replace this example code with whatever you need
            return $this->render('default/home1.html.twig', array(
                'base_dir' => realpath($this->container->getParameter('kernel.root_dir') . '/..'),
            ));
        }
    }

    /**
     * @Route("/about/{section}", name="aboutpage" )
     */
    public function aboutAction($section){
        if($section == "staff"){

            $staff =  $this->getDoctrine()
            ->getRepository('AppBundle:StaffMember')
            ->findAll();


            return $this->render('default/about/staff.html.twig',[
                'staff'=>$staff
                ]);
        }else if($section == "brew_testament"){
            return $this->render('default/about/brew_testament.html.twig');

        }
        return $this->render('default/about/history.html.twig');
    }

    /**
     * @Route("/ajax/{no}", name="slidepage")
     */
    public function auxAction($no){

        return $this->render("test.html.twig",["number"=> $no]);

    }




}
