<?php
/**
 * Created by PhpStorm.
 * User: Mercy
 * Date: 7/27/2016
 * Time: 8:15 PM
 */

namespace AppBundle\Controller;


use AppBundle\Entity\ContactRequest;
use AppBundle\Entity\Shop;
use AppBundle\Form\ContactRequestType;
use Doctrine\DBAL\Types\DateType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;

class ContactController extends Controller
{
    /**
     * @Route("/contact", name="contactpage")
     */
    public function contactAction(Request $request)
    {



        $wedding =  $request->get('wedding');

        $contact_request = new ContactRequest();
        $shops = $this->get('doctrine')->getEntityManager()->getRepository(Shop::class)->findAll();
        $form = $this->createForm(ContactRequestType::class, $contact_request, array('shops'=>$shops));


        if($wedding){
            $form->add('date','date', ['widget'=>'single_text']);

        }


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contact_request->setCreatedAt(new \DateTime());
            $em = $this->getDoctrine()->getManager();
            $em->persist($contact_request);
            $em->flush();


            $message_content = $this->render("emails/request_template.html.twig",[
                'enquiry' => $contact_request->getEnquiry(),
                'shop' => $contact_request->getShop(),
                'date' => $contact_request->getDate()
            ]);
            $message = Swift_Message::newInstance()
                ->setFrom('support@bottleapostle.com')
                ->setTo($contact_request->getEmail())
                ->setSubject('Contact Request')
                ->setBody($message_content,"text/html");


            $mailer = $this->get('mailer');
            //Pass a variable name to the send() method
            if (!$mailer->send($message, $failures))
            {
                echo "Failures:";
                var_dump($failures);
                die();
            }



            if($request->isXmlHttpRequest()){
                return new JsonResponse(["success"=>true]);
            }
            return $this->redirectToRoute('contact_request_success');
        }




        return $this->render('default/contact/contact.html.twig',[
            'wedding' => $wedding,
            'errors' => $form->getErrorsAsString()
        ]);

    }


    public function contactFormAction($wedding = false){



        $request = $this->get('request');
        $contact_request = new ContactRequest();

        $shops = $this->get('doctrine')->getEntityManager()->getRepository(Shop::class)->findAll();

        $form = $this->createForm(ContactRequestType::class, null, array('shops'=>$shops));


        if($wedding){
            $form->add('date','date', ['widget'=>'single_text']);

        }

         $form->handleRequest($request);
         if ($form->isSubmitted() && $form->isValid()) {
             $contact_request->setDate(new \DateTime());

             $em = $this->getDoctrine()->getManager();
             $em->persist($contact_request);
             $em->flush();

         }

         return $this->render('components/contact-form.html.twig',[
         'form'=>$form->createView(),
         'wedding'=> $wedding]);
    }

    /**
     * @Route("/contact/success", name="contact_request_success")
     */
    function contactSuccessAction()
    {

        return $this->render(':default/contact:contact-success.html.twig');

    }

    function newsletterSubcriptionAction(){
        //render the newsletter form


    }
    /**
     * @param Request $request
     * @Route("/location",name="locationpage")
     */
    public function locationAction()
    {
        $shopRepository = $this->getDoctrine()->getRepository(Shop::class);
        $shops = $shopRepository->findAll();

        return $this->render('default/location/location.html.twig',[
            'shops' => $shops
        ]);

    }
    public function infoWindowAction($shop)
    {

        return $this->render('components/map-info-window.html.twig',[
            'shop' => $shop
        ]);

    }


}