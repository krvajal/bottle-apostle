<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;


class WeddingController extends Controller
{

    /**
     * @return Response
     * @Route("/events/wedding",name="events_wedding_home")
     */
    function indexAction()
    {

        return $this->render('default/events/wedding/index.html.twig');
    }


    /**
     * @return Response
     * @Route("/events/wedding/refertofriend",name="events_wedding_process_1")
     */
    function process1Action(){
        return $this->render('default/events/wedding/process1/index.html.twig');

    }
    /**
     * @return Response
     * @Route("/events/wedding/enquiry",name="events_wedding_process_2")
     */
    function process2Action(){

        //TODO: change icon images for high resolution ones
        return $this->render('default/events/wedding/process2/index.html.twig');

    }
}