<?php
/**
 * Created by PhpStorm.
 * User: carvajal
 * Date: 8/26/16
 * Time: 1:18 PM
 */

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin as AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;



class OpeningAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {

        $options  = ['choices' => array(
        'Mon'    => 'Monday',
        'Tue'    => 'Tuesday',
        'Wed'    => 'Wednesday',
        'Thu'    => 'Thursday',
        'Fri'    => 'Friday',
        'Sat'    => 'Saturday',
        'Sun'    => 'Sunday',
        )];
        $formMapper

            ->add('day','choice',
               $options)

            ->add('from','time',['help' => 'Store opening time for selected day'])
            ->add('to','time',['help' => 'Store closing time for selected day'])
            ->end();


    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {


    }

    protected function configureListFields(ListMapper $listMapper)
    {


    }


}