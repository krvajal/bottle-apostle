<?php


namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin as AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;


class ContactRequestAdmin extends  AbstractAdmin{


    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
        
        ->add('enquiry', 'text')
        ->add('email','email')  
       ->add('shop','sonata_type_model',['property'=>'name'])
       ->add('message','textarea')
       ->add('date','sonata_type_datetime_picker');
        



    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('enquiry');
        $datagridMapper->add("email");
        $datagridMapper->add("createdAt");
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('enquiry');
        $listMapper->add("email");
        $listMapper->add("createdAt");
       

    }


     protected function configureShowFields(ShowMapper $showMapper){

       $showMapper
       ->add('title');

     }

}