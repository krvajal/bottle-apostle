<?php


namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin as AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;



class StaffMemberAdmin extends  AbstractAdmin{


    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
        
        ->add('firstName', 'text')
        ->add('lastName', 'text')
       ->add('picture', 'sonata_media_type', array(
                 'provider' => 'sonata.media.provider.image',
                 'context' => 'default'
            ))
       ->add('description','textarea');
       
        



    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('firstName');
        $datagridMapper->add("lastName");
        
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('firstName','string');
        $listMapper->add("lastName",'string');
        $listMapper->add("picture",'string');
          
    }
    // protected function configureShowFields(ShowMapper $showMapper){

    //   $showMapper
    //   ->add('title')
    //   ->add('free','boolean');
    // }

}