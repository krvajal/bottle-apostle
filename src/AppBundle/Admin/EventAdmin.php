<?php


namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin as AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;



class EventAdmin extends  AbstractAdmin{


    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
        ->with('General')
        ->add('title', 'text')

       ->add('image', 'sonata_media_type', array(
                 'provider' => 'sonata.media.provider.image',
                 'context' => 'default'
            ))
       ->add('description','textarea')
       ->add('gallery', 'sonata_type_model_list', array('required' => false), array('link_parameters'   => array('context' => 'default')))
       ->end()
       ->with("Dates")
       ->add('from_date','sonata_type_datetime_picker')
       ->add('to_date','sonata_type_datetime_picker')
       ->end() 
       ->with("Places")
       ->add('tickets','integer')
       ->add('price','number')
       ->add('location','text')
       ->end();
        



    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('title');
        $datagridMapper->add("location");
        $datagridMapper->add("price");
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('title');
        $listMapper->add("location");
        $listMapper->add("price");
        $listMapper->add("tickets");
        $listMapper->add("free");
        $listMapper->add("from_date",'datetime',array('date_format' => 'yyyy-MM-dd HH:mm:ss'));
        $listMapper->add("to_date",'datetime',array('date_format' => 'yyyy-MM-dd HH:mm:ss'));
        $listMapper->add('image','sonata_media_type');

    }
    // protected function configureShowFields(ShowMapper $showMapper){

    //   $showMapper
    //   ->add('title')
    //   ->add('free','boolean');
    // }

}