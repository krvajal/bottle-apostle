<?php
/**
 * Created by PhpStorm.
 * User: carvajal
 * Date: 8/26/16
 * Time: 1:18 PM
 */

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\Admin as AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;



class ShopAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper

            ->add('name', 'text')
            ->add('email','email')
            ->add('picture', 'sonata_type_model_list', array(
                'required' => false,
            ), array(
                'link_parameters' => array(
                    'context' => 'default',
                    'filter' => array('context' => array('value' => 'defaul')),
                    'provider' => '',
                )
            ))
            ->add('address','sonata_type_model',['class'=> 'Application\Sonata\CustomerBundle\Entity\Address','property'=>'name'])
            ->add('phone','text')
            ->add('latitude',"number")
            ->add('longitude',"number")->end();

        $options =  array('label' => "Opening times",'by_reference'=>false);

        $formMapper->with('Openings',['description'=> "Please enter the days the store will be open" ])
            ->add('openings', 'sonata_type_collection',$options,
                ['edit'=>'inline', 'inline'=>'table', 'sortable'=>'pos']
            )
            ->end();





    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
        $datagridMapper->add('name');
        $datagridMapper->add('email');



    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add("id");
        $listMapper->addIdentifier('name');
        $listMapper->addIdentifier('email');
        $listMapper->addIdentifier('picture');



    }


}