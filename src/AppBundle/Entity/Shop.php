<?php
/**
 * Created by PhpStorm.
 * User: carvajal
 * Date: 8/26/16
 * Time: 12:54 PM
 */

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

use Application\Sonata\CustomerBundle\Entity\Address;

/**
 * @ORM\Entity(repositoryClass="ContactRequestRepository")
 * @ORM\Table(name="ba_shop")
 */
class Shop
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @var string
     * @ORM\Column(type= "string", length =200)
     *
     */
    protected $name;
    /**
     * @ORM\OneToOne(targetEntity="Application\Sonata\CustomerBundle\Entity\Address")
     * @ORM\JoinColumn(name="address_id", referencedColumnName="id")
     */
    protected $address;
    /**
     * @var string
     * @ORM\Column(type= "string", length =200)
     *
     */
    protected $phone;
    /**
     * @var string
     * @ORM\Column(type= "string", length = 100)
     *
     */
    protected $email;
    /**
     * @var string
     * @ORM\ManyToMany(targetEntity="Opening",cascade={"persist"})
     * @ORM\JoinTable(name="shop_openning",
     *      joinColumns={@ORM\JoinColumn(name="shop_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="opening_id", referencedColumnName="id")}
     *      )
     */
     
    protected $openings;

    /**
     * @ORM\Column(type= "decimal",precision=20,scale=10)
     *
     */
    protected $latitude;

    /**
     * @ORM\Column(type= "decimal", precision=20,scale=10)
     *
     */
    protected  $longitude;

    /**
     * @var \Application\Sonata\MediaBundle\Entity\Media
     * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     */

    private $picture;

    /**
     * Constructor
     */
    public function __construct()
    {

        $this->openings = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Shop
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Shop
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Shop
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Add address
     *
     * @param \Application\Sonata\CustomerBundle\Entity\Address $address
     *
     * @return Shop
     */
    public function setAddress(\Application\Sonata\CustomerBundle\Entity\Address $address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Remove address
     *
     * @param \Application\Sonata\CustomerBundle\Entity\Address $address
     */
    public function removeAddress(\Application\Sonata\CustomerBundle\Entity\Address $address)
    {
        $this->address->removeElement($address);
    }

    /**
     * Get address
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Add opening
     *
     * @param \AppBundle\Entity\Opening $opening
     *
     * @return Shop
     */
    public function addOpening(\AppBundle\Entity\Opening $opening)
    {
        $this->openings[] = $opening;

        return $this;
    }

    /**
     * Remove opening
     *
     * @param \AppBundle\Entity\Opening $opening
     */
    public function removeOpening(\AppBundle\Entity\Opening $opening)
    {
        $this->openings->removeElement($opening);
    }

    /**
     * Get openings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOpenings()
    {
        return $this->openings;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     *
     * @return Shop
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     *
     * @return Shop
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set picture
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $picture
     *
     * @return Shop
     */
    public function setPicture(\Application\Sonata\MediaBundle\Entity\Media $picture = null)
    {
        $this->picture = $picture;

        return $this;
    }

    /**
     * Get picture
     *
     * @return \Application\Sonata\MediaBundle\Entity\Media
     */
    public function getPicture()
    {
        return $this->picture;
    }
}
