<?php
/**
 * Created by PhpStorm.
 * User: carvajal
 * Date: 8/26/16
 * Time: 12:58 PM
 */

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
/**
 * Class Opening
 * @package AppBundle\Entity
 * @ORM\Entity()
 * @ORM\Table(name="ba_shop_opening")
 */

class Opening
{


    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type= "string", length =50,name="_day")
     *
     */
    protected $day;
    /**
     * @ORM\Column(type="datetime", name="_from")
     */
    protected $from;
    /**
     * @ORM\Column(type="datetime", name= "_to")
     */
    protected  $to;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set from
     *
     * @param \DateTime $from
     *
     * @return Opening
     */
    public function setFrom($from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * Get from
     *
     * @return \DateTime
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Set to
     *
     * @param \DateTime $to
     *
     * @return Opening
     */
    public function setTo($to)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * Get to
     *
     * @return \DateTime
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @return mixed
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * @param mixed $day
     */
    public function setDay($day)
    {
        $this->day = $day;
    }

}
