<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="ContactRequestRepository")
 * @ORM\Table(name="contact_request")
 */
class ContactRequest
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="EnquiryType")
     */
    private $enquiry;
    /**
     * @ORM\ManyToOne(targetEntity="Shop")
     */
    private $shop;
    /**
     * @ORM\Column(type="string",length=100)
     * @Assert\NotBlank()
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     *
     */
    private $email;
    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     *
     */
    private $message;


    /**
     * @ORM\Column(type="date",nullable=true)
     */

    private $date;
    /**
     * @ORM\Column(type="date")
     */
    private $createdAt;

    /**
     * @return Date
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    public function getDate()
    {

        return $this->date;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getShop()
    {
        return $this->shop;

    }

    public function getMessage()
    {
        return $this->message;
    }

    public function getEnquiry()
    {
        return $this->enquiry;
    }

    /**
     * @param mixed $enquiry
     */
    public function setEnquiry($enquiry)
    {
        $this->enquiry = $enquiry;
    }

    /**
     * @param mixed $shop
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
