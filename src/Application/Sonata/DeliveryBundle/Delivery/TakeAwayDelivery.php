<?php

namespace Application\Sonata\DeliveryBundle\Delivery;

use Sonata\Component\Delivery\BaseServiceDelivery;


/**
 * Class TakeAwayDelivery
 */
class TakeAwayDelivery extends BaseServiceDelivery
{


    public function  __construct()
    {
        $this->setPrice(0);
        $this->setEnabled(true);
    }

    /**
     * {@inheritdoc}
     */
    public function isAddressRequired()
    {
        return false;
    }


    /**
     * {@inheritdoc}
     */
    public function getCode()
    {
        return 'take_away';
    }
    public  function  getName()
    {
        return "Take away";
    }
}