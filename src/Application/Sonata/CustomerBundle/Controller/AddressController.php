<?php


namespace Application\Sonata\CustomerBundle\Controller;


use Application\Sonata\CustomerBundle\Entity\Address;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sonata\Component\Customer\AddressInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class AddressController extends Controller{


    /**
     * @Route("/api/v1/addresses")
     */
    public function getAddressListRestAction(){

        $request =  $this->get('request');
        $type = $request->get("type","billing");
        $customerid = $request->get("cust","");
        $customer = $this->get('sonata.customer.manager')->find($customerid);
        if($customer == null){
            throw new NotFoundHttpException("customer not found");

        }else{
            //customer found
            $addresses = [];
            if($type  == "billing") {
                $addresses = $customer->getAddressesByType(AddressInterface::TYPE_BILLING);

            }else if ($type == "delivery"){
                $addresses = $customer->getAddressesByType(AddressInterface::TYPE_DELIVERY);
            }

            $addresses_array = [];
            //convert the addresses to an array,
            // this will be normaly done using a serializer
            // but doing it by hand now
            foreach ($addresses as $address){
                $addresses_array[] = $address->getAddressArrayForRender();
            }

            return new JsonResponse($addresses_array);

        }


    }

}