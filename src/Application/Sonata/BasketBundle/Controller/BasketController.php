<?php

/*
 * This file is part of the Sonata Project package.
 *
 * (c) Thomas Rabaix <thomas.rabaix@sonata-project.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Application\Sonata\BasketBundle\Controller;

use Abraham\TwitterOAuth\Request;
use AppBundle\Payment\StripePayment;
use Application\Sonata\CustomerBundle\Entity\Address;
use Application\Sonata\CustomerBundle\Entity\Customer;
use Instaphp\Exceptions\Exception;
use Sonata\Component\Basket\BasketInterface;
use Sonata\Component\Customer\AddressInterface;
use Sonata\Component\Delivery\UndeliverableCountryException;
use Sonata\Component\Payment\Paypal;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DomCrawler\Form;
use Symfony\Component\Form\Extension\Validator\ViolationMapper\ViolationMapper;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use \Stripe\Customer as StripeCustomer;


/**
 * This controller manages the Basket operation and most of the order process.
 */
class BasketController extends Controller
{
    /**
     * Shows the basket.
     *
     * @param Form $form
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction($form = null)
    {


        $form = $form ?: $this->createForm('sonata_basket_basket', $this->get('sonata.basket'), array(
            'validation_groups' => array('elements'),
        ));

        // always validate the basket
        if (!$form->isBound()) {
            if ($violations = $this->get('validator')->validate($form)) {
                $violationMapper = new ViolationMapper();
                foreach ($violations as $violation) {
                    $violationMapper->mapViolation($violation, $form, true);
                }
            }
        }

        $this->get('session')->set('sonata_basket_delivery_redirect', 'sonata_basket_delivery_address');

        $this->get('sonata.seo.page')->setTitle($this->get('translator')->trans('basket_index_title', array(), 'SonataBasketBundle'));

         return $this->render('components/cart-dropdown-content.twig',[
            'basket'=> $this->get('sonata.basket'),
            'form' => $form->createView()
            ]);
        
    }

    public function  checkoutStepperAction(){

        $billingAddressForm =$this->createForm('sonata_basket_address', null, array('addresses' => null));
        $deliveryAddressForm = $this->createForm('sonata_basket_address', null, array('addresses' => null));


        return $this->render('components/checkout-sidebar.html.twig',[
            'basket'=> $this->get('sonata.basket'),
            'billingAddressForm' => $billingAddressForm,
            'deliveryAddressFomr' => $deliveryAddressForm
        ]);
    }



    /**
     * Update basket form rendering & saving.
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function updateAction()
    {
        $form = $this->createForm('sonata_basket_basket', $this->get('sonata.basket'), array('validation_groups' => array('elements')));
        $form->bind($this->get('request'));

        if ($form->isValid()) {
            $basket = $form->getData();
            $basket->reset(false); // remove delivery and payment information
            $basket->clean(); // clean the basket

            // update the basket
            $this->get('sonata.basket.factory')->save($basket);

            return new RedirectResponse($this->generateUrl('sonata_basket_index'));
        }

        return $this->forward('SonataBasketBundle:Basket:index', array(
            'form' => $form,
        ));
    }

    /**
     * Adds a product to the basket.
     *
     * @throws MethodNotAllowedException
     * @throws NotFoundHttpException
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addProductAction()
    {

        
        
        $request = $this->get('request');
        $params = $request->get('add_basket');

        if ($request->getMethod() != 'POST') {
            throw new MethodNotAllowedException(array('POST'));
        }

        // retrieve the product
        $product = $this->get('sonata.product.set.manager')->findOneBy(array('id' => $params['productId']));

        if (!$product) {
            throw new NotFoundHttpException(sprintf('Unable to find the product with id=%d', $params['productId']));
        }


        // retrieve the custom provider for the product type
        $provider = $this->get('sonata.product.pool')->getProvider($product);


        $formBuilder = $this->get('form.factory')->createNamedBuilder('add_basket', 'form', null, array(
            'data_class' => $this->container->getParameter('sonata.basket.basket_element.class'),
            'csrf_protection' => false,
        ));



        $provider->defineAddBasketForm($product, $formBuilder);

        // load and bind the form
        $form = $formBuilder->getForm();
        $form->bind($request);




        // if the form is valid add the product to the basket
        if ($form->isValid()) {
            $basket = $this->get('sonata.basket');
            $basketElement = $form->getData();

            $quantity = $basketElement->getQuantity();
            $currency = $this->get('sonata.basket')->getCurrency();
            $price = $provider->calculatePrice($product, $currency, true, $quantity);

            if ($basket->hasProduct($product)) {
                $basketElement = $provider->basketMergeProduct($basket, $product, $basketElement);
            } else {
                $basketElement = $provider->basketAddProduct($basket, $product, $basketElement);
            }

            $this->get('sonata.basket.factory')->save($basket);

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse([
                    'success' => true,
                    'basketElement' => $basketElement,
                    'locale' => $basket->getLocale(),
                    'product' => $product,
                    'price' => $price,
                    'currency' => $currency,
                    'quantity' => $quantity,
                    'provider' => $provider,
                ]);
            }
            else{

                return new RedirectResponse($this->generateUrl('sonata_basket_index'));    
            }

            
        }


        if($request->isXmlHttpRequest()){
                return new JsonResponse([
                    'success' => false,
                    'errors'=> $form->getErrors(true, false)
                ]);

        }



        // an error occur, forward the request to the view
        return $this->forward('SonataProductBundle:Product:view', array(
            'productId' => $product,
            'slug' => $product->getSlug(),
        ));
    }

    /**
     * Resets (empties) the basket.
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function resetAction()
    {
        $this->get('sonata.basket.factory')->reset($this->get('sonata.basket'));

        return new RedirectResponse($this->generateUrl('sonata_basket_index'));
    }

    /**
     * Displays a header preview of the basket.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showFinalBasketAction()
    {
        return $this->render('components/final-order-basket-content.html.twig', array(
            'basket' => $this->get('sonata.basket'),
        ));
    }

    /**
     * Order process step 1: retrieve the customer associated with the logged in user if there's one
     * or create an empty customer and put it in the basket.
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function authenticationStepAction()
    {
        $customer = $this->get('sonata.customer.selector')->get();

        $basket = $this->get('sonata.basket');
        $basket->setCustomer($customer);

        $this->get('sonata.basket.factory')->save($basket);

        return new RedirectResponse($this->generateUrl('sonata_basket_delivery_address'));
    }

    /**
     * Order process step 5: choose payment mode.
     *
     * @throws HttpException
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function paymentStepAction()
    {
        $basket = $this->get('sonata.basket');

        if ($basket->countBasketElements() == 0) {
            return new RedirectResponse($this->generateUrl('sonata_basket_index'));
        }

        $customer = $basket->getCustomer();

        if (!$customer) {
            throw new HttpException('Invalid customer');
        }

        if (null === $basket->getBillingAddress()) {
            // If no payment address is specified, we assume it's the same as the delivery
            $billingAddress = clone $basket->getDeliveryAddress();
            $billingAddress->setType(AddressInterface::TYPE_BILLING);
            $basket->setBillingAddress($billingAddress);
        }

        $form = $this->createForm('sonata_basket_payment', $basket, array(
            'validation_groups' => array('delivery'),
        ));

        if ($this->get('request')->getMethod() == 'POST') {
            $form->bind($this->get('request'));

            if ($form->isValid()) {
                // save the basket
                $this->get('sonata.basket.factory')->save($basket);

                return new RedirectResponse($this->generateUrl('sonata_basket_final'));
            }
        }

        $this->get('sonata.seo.page')->setTitle($this->get('translator')->trans('basket_payment_title', array(), 'SonataBasketBundle'));

        return $this->render('SonataBasketBundle:Basket:payment_step.html.twig', array(
            'basket' => $basket,
            'form' => $form->createView(),
            'customer' => $customer,
        ));
    }




    /**
     * Order process step 5: choose payment mode.
     *
     * @throws HttpException
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function paymentStepRestAction()
    {

        $request = $this->get('request');



        if (!($request->getMethod() == 'POST')){

            throw new MethodNotAllowedHttpException();

        }


        $basket = $this->get('sonata.basket');

        if ($basket->countBasketElements() == 0) {
            return new HttpException("Basket empty");
        }

        $customer = $basket->getCustomer();


        if (!$customer) {
            throw new HttpException('Invalid customer');
        }

        if( null === $basket->getBillingAddress()){

            throw new HttpException('not billing address');
        }


        $methods = $this->get('sonata.payment.selector')->getAvailableMethods($basket, $basket->getDeliveryAddress());

        if(empty($methods)){
            return new JsonResponse([
                'success'=>false,
                'message'=> 'not payment methods available'
            ]);

        }

        $params = array();
        $content = $request->getContent();
        if (!empty($content))
        {
            $params = json_decode($content, true); // 2nd param to get as array
        }else{

            throw new \HttpException("Not data sent to us");
        }
        //default to stripe
        $methodCode = $params["payment_method"];
        $choices = array();

        foreach ($methods as $method) {
            $choices[$method->getCode()] = $method;
        }

        //set the correct payment method
        $basket->setPaymentMethod($choices[$methodCode]);
        $basket->setPaymentMethodCode($methodCode);

        if($methodCode ==  'stripe'){

            $this->createStripeCustomer($params["stripe_token"], $customer, $basket);

        }

        $this->get('sonata.basket.factory')->save($basket);

        return new JsonResponse([
            'success' => true
        ]);


    }

    public function  deliveryRestStepAction(){
        $request = $this->get('request');



            $basket = $this->get('sonata.basket');

            if ($basket->countBasketElements() == 0) {
                return new HttpException("Basket empty");
            }

            $customer = $basket->getCustomer();

            if (!$customer) {
                throw new NotFoundHttpException('Not customer');
            }

            $deliveryPool = $this->get("sonata.delivery.pool");
            $deliverySelector = $this->get("sonata.delivery.selector");
            $methods = $deliverySelector->getAvailableMethods($basket, $basket->getDeliveryAddress());



            if (count($methods) === 0) {
                throw new UndeliverableCountryException($basket->getDeliveryAddress());
            }
        if($request->getMethod() == 'GET'){
            $choices = array();
            foreach ($methods as $method) {
                $choices[$method->getCode()] = $method->getName();
            }


                return new JsonResponse($choices);
        }else{

            $basket->setDeliveryMethod($methods[0]);

            $this->get('sonata.basket.factory')->save($basket);


            return new JsonResponse([
               "success"=>true,
                "method"=>"take_away"
            ]);
        }

    }

    /**
     * Order process step 2: choose an address from existing ones or create a new one.
     *
     * @throws NotFoundHttpException
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function deliveryAddressRestStepAction()
    {


        $request = $this->get('request');
        if( $request->getMethod() != 'POST'){
            throw new NotFoundHttpException("invalid method");
        }
        $params = array();
        $content = $request->getContent();
        if (!empty($content))
        {
            $params = json_decode($content, true); // 2nd param to get as array
        }else{

            throw new \HttpException("Not data sent to us");
        }




        $customer = $this->get('sonata.customer.selector')->get();

        if (!$customer) {
            throw new NotFoundHttpException('customer not found');
        }

        $basket = $this->get('sonata.basket');
        $basket->setCustomer($customer);

        if ($basket->countBasketElements() == 0) {
            return new RedirectResponse($this->generateUrl('sonata_basket_index'));
        }

        $addresses = $customer->getAddressesByType(AddressInterface::TYPE_DELIVERY);
        //null the addresses for now, this is not specified in the design




        $address = new Address();
        $address->setAddress1($params['address1']);
        $address->setAddress2($params['address2']);
        $address->setAddress3($params['address3']);
        $address->setCity($params['city']);
        $address->setCountryCode($params['country']);
        $address->setPostcode($params['postcode']);

        $address->setType(AddressInterface::TYPE_DELIVERY);


        $customer->addAddress($address);

        $this->get('sonata.customer.manager')->save($customer);

        $this->get('session')->getFlashBag()->add('sonata_customer_success', 'address_add_success');

        $basket->setDeliveryAddress($address);

        $this->get('sonata.basket.factory')->save($basket);
        //address added sucessfully
        return new JsonResponse([
            'success' => true

        ]);

    }

    public function  paymentAddressRestStepAction()
    {


        $request = $this->get('request');
        if( $request->getMethod() != 'POST'){
            throw new NotFoundHttpException("invalid method");
        }

        $params = array();
        $content = $request->getContent();
        if (!empty($content))
        {
            $params = json_decode($content, true); // 2nd param to get as array
        }else{

            throw new \HttpException("Not data sent to us");
        }


        $customer = $this->get('sonata.customer.selector')->get();

        if (!$customer) {
            throw new NotFoundHttpException('customer not found');
        }

        $basket = $this->get('sonata.basket');
        $basket->setCustomer($customer);

        if ($basket->countBasketElements() == 0) {
            return new RedirectResponse($this->generateUrl('sonata_basket_index'));
        }

        $addresses = $customer->getAddressesByType(AddressInterface::TYPE_BILLING);
        //null the addresses for now, this is not specified in the design
        $address =  null;

        $address = new Address();
        $address->setAddress1($params['address1']);
        $address->setAddress2($params['address2']);
        $address->setAddress3($params['address3']);
        $address->setCity($params['city']);
        $address->setCountryCode($params['country']);
        $address->setPostcode($params['postcode']);

        $address->setType(AddressInterface::TYPE_BILLING);

        //persist address

        $em = $this->getDoctrine()->getManager();
        $em->persist($address);
        $em->flush();

        $this->get('sonata.customer.manager')->save($customer);

        $this->get('session')->getFlashBag()->add('sonata_customer_success', 'address_add_success');

        $customer->addAddress($address);
        $basket->setBillingAddress($address);


        $this->get('sonata.basket.factory')->save($basket);


        //address added sucessfully
        return new JsonResponse([
            'success' => true
        ]);

    }


    /**
     * Order process step 3: choose delivery mode.
     *
     * @throws NotFoundHttpException
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function deliveryStepAction()
    {


        $basket = $this->get('sonata.basket');

        if ($basket->countBasketElements() == 0) {

            return new RedirectResponse($this->generateUrl('sonata_basket_index'));
        }

        $customer = $basket->getCustomer();

        if (!$customer) {
            throw new NotFoundHttpException('customer not found');
        }

        try {

            $form = $this->createForm('sonata_basket_shipping', $basket, array(
                'validation_groups' => array('delivery'),
            ));
            

        } catch (UndeliverableCountryException $ex) {

             
            
            $countryName = Intl::getRegionBundle()->getCountryName($ex->getAddress()->getCountryCode());
            
            $message = $this->get('translator')->trans('undeliverable_country', array('%country%' => $countryName), 'SonataBasketBundle');
            var_dump($message);
            die();
            $this->get('session')->getFlashBag()->add('error', $message);

            return new RedirectResponse($this->generateUrl('sonata_basket_index'));
        }

        $template = 'SonataBasketBundle:Basket:delivery_step.html.twig';

        if ($this->get('request')->getMethod() == 'POST') {
            $form->bind($this->get('request'));

            if ($form->isValid()) {
                // save the basket
                $this->get('sonata.basket.factory')->save($form->getData());

                return new RedirectResponse($this->generateUrl('sonata_basket_payment_address'));
            }
        }

        $this->get('sonata.seo.page')->setTitle($this->get('translator')->trans('basket_delivery_title', array(), 'SonataBasketBundle'));

        return $this->render($template, array(
            'basket' => $basket,
            'form' => $form->createView(),
            'customer' => $customer,
        ));
    }

    /**
     * Order process step 2: choose an address from existing ones or create a new one.
     *
     * @throws NotFoundHttpException
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function deliveryAddressStepAction()
    {


        $customer = $this->get('sonata.customer.selector')->get();

        if (!$customer) {
            throw new NotFoundHttpException('customer not found');
        }


        $basket = $this->get('sonata.basket');
        $basket->setCustomer($customer);

        if ($basket->countBasketElements() == 0) {

            return new RedirectResponse($this->generateUrl('sonata_basket_index'));
        }

        $addresses = $customer->getAddressesByType(AddressInterface::TYPE_DELIVERY);

        // Show address creation / selection form
        $form = $this->createForm('sonata_basket_address', null, array('addresses' => $addresses));


        $template = 'SonataBasketBundle:Basket:delivery_address_step.html.twig';

        if ($this->get('request')->getMethod() == 'POST') {
            $form->bind($this->get('request'));

            if ($form->isValid()) {
                if ($form->has('useSelected') && $form->get('useSelected')->isClicked()) {
                    $address = $form->get('addresses')->getData();
                } else {
                    $address = $form->getData();
                    $address->setType(AddressInterface::TYPE_DELIVERY);
                        
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($address);
                    $em->flush();

                    $customer->addAddress($address);

                    $this->get('sonata.customer.manager')->save($customer);

                    $this->get('session')->getFlashBag()->add('sonata_customer_success', 'address_add_success');
                }

                $basket->setCustomer($customer);
                $basket->setDeliveryAddress($address);
                // save the basket
                $this->get('sonata.basket.factory')->save($basket);

                return new RedirectResponse($this->generateUrl('sonata_basket_delivery'));
            }
        }

        // Set URL to be redirected to once edited address
        $this->get('session')->set('sonata_address_redirect', $this->generateUrl('sonata_basket_delivery_address'));

        $this->get('sonata.seo.page')->setTitle($this->get('translator')->trans('basket_delivery_title', array(), 'SonataBasketBundle'));

        return $this->render($template, array(
            'form' => $form->createView(),
            'addresses' => $addresses,
            'basket' => $basket,
        ));
    }

    /**
     * Order process step 4: choose a billing address from existing ones or create a new one.
     *
     * @throws NotFoundHttpException
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function paymentAddressStepAction()
    {

        $basket = $this->get('sonata.basket');

        if ($basket->countBasketElements() == 0) {
            return new RedirectResponse($this->generateUrl('sonata_basket_index'));
        }

        $customer = $basket->getCustomer();

        if (!$customer) {
            throw new NotFoundHttpException('customer not found');
        }

        $addresses = $customer->getAddressesByType(AddressInterface::TYPE_BILLING);

        // Show address creation / selection form
        $form = $this->createForm('sonata_basket_address', null, array('addresses' => $addresses->toArray()));


        
        $template = 'SonataBasketBundle:Basket:payment_address_step.html.twig';

        if ($this->get('request')->getMethod() == 'POST') {
            $form->bind($this->get('request'));

            if ($form->isValid()) {
                if ($form->has('useSelected') && $form->get('useSelected')->isClicked()) {
                    $address = $form->get('addresses')->getData();
                } else {
                    $address = $form->getData();
                    $address->setType(AddressInterface::TYPE_BILLING);

                     $em = $this->getDoctrine()->getManager();
                    $em->persist($address);
                    $em->flush();

                    
                    $customer->addAddress($address);

                    $this->get('sonata.customer.manager')->save($customer);

                    $this->get('session')->getFlashBag()->add('sonata_customer_success', 'address_add_success');
                }

                $basket->setCustomer($customer);
                $basket->setBillingAddress($address);
                // save the basket
                $this->get('sonata.basket.factory')->save($basket);

                return new RedirectResponse($this->generateUrl('sonata_basket_payment'));
            }
        }

        // Set URL to be redirected to once edited address
        $this->get('session')->set('sonata_address_redirect', $this->generateUrl('sonata_basket_payment_address'));

        $this->get('sonata.seo.page')->setTitle($this->get('translator')->trans('basket_payment_title', array(), 'SonataBasketBundle'));

        return $this->render($template, array(
            'form' => $form->createView(),
            'addresses' => $addresses,
        ));
    }

    /**
     * Order process step 6: order's review & conditions acceptance checkbox.
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function finalReviewStepAction()
    {


        $basket = $this->get('sonata.basket');


        $violations = $this
            ->get('validator')
            ->validate($basket, array('delivery', 'payment'));

        if ($violations->count() > 0) {
            // basket not valid


            // todo : add flash message rendering in template
            foreach ($violations as $violation) {
               
                
                $this->get('session')->getFlashBag()->add('error', 'Error: '.$violation->getMessage());
                

            }

            return new Response("Invalid basket");

            return new RedirectResponse($this->generateUrl('sonata_basket_index'));
        }



        if ($this->get('request')->getMethod() == 'POST') {
            if ($this->get('request')->get('tac')) {
                // send the basket to the payment callback

                return $this->forward('SonataPaymentBundle:Payment:sendbank');
            }
        }

        $this->get('sonata.seo.page')->setTitle($this->get('translator')->trans('basket_review_title', array(), 'SonataBasketBundle'));

        return $this->render('SonataBasketBundle:Basket:final_review_step.html.twig', array(
            'basket' => $basket,
            'tac_error' => $this->get('request')->getMethod() == 'POST',
        ));
    }
    //the payment method choosen was stripe so handle it here

    private function createStripeCustomer($stripe_token, Customer $customer, BasketInterface $basket)
    {

        $stripe_customer_id = $customer->getStripeId();
        if($stripe_customer_id != null && strlen($stripe_customer_id) > 0){
            //we already have a customer, just attach the token to it

            try {
                $stripeCustomer = StripeCustomer::retrieve( ["id"=>$stripe_customer_id,  "expand" => ["default_source"]] );
                $card = $stripeCustomer->sources->create(array("source"=>$stripe_token));





                    //some words on stripe api
                //RETRIEVE A CARD
                // $card = $customer->sources->retrieve({CARD_ID});


                /// UPDATE CARD
                ///   $card->name = {NEW_NAME};
                ///  $card->save();
                //Stripe\Card JSON: {
                //  "id": "card_19FVJ4BrNgTWKZ2oPQK7DUMs",
                //  "object": "card",
                //  "address_city": null,
                //  "address_country": null,
                //  "address_line1": " Telewest Midlands",
                //  "address_line1_check": "pass",
                //  "address_line2": null,
                //  "address_state": null,
                //  "address_zip": "EN77 1ZA",
                //  "address_zip_check": "pass",
                //  "brand": "Visa",
                //  "country": "US",
                //  "customer": "cus_9YcYxSw8bqUJHG",
                //  "cvc_check": "pass",
                //  "dynamic_last4": null,
                //  "exp_month": 3,
                //  "exp_year": 2018,
                //  "funding": "credit",
                //  "last4": "4242",
                //  "metadata": {
                //                },
                //  "name": "David Moore",
                //  "tokenization_method": null
                //}

                //DELETE CARD
                ///$customer->sources->retrieve({CARD_ID})->delete();

                ///GET A LIST OF CARDS
                /// \Stripe\Customer::retrieve({CUSTOMER_ID})->sources->all(array(
                ///"object" => "card"));


            }catch (\Exception $e){
                return new HttpException("Failed to load stripe customer");

            }


        }else {


            \Stripe\Stripe::setApiKey("sk_test_fINM3JLxGWTnvAFEG1Gl2fHj");
            $stripeCustomer = StripeCustomer::create(array(
                "source" => $stripe_token,
                "description" => $customer->getId(),
                "email" => ($customer->getUser()) ? ($customer->getUser()->getEmail()) : "",
                "metadata" => [
                    'customer_id' => $customer->getId(),
                    'customer_first_name' => $customer->getFirstname(),
                    'customer_last_name' => $customer->getLastname(),
                    'customer_user_email' => ($customer->getUser()) ? ($customer->getUser()->getEmail()) : ""
                ]));

            $customer->setStripeId($stripeCustomer->id);

            $this->get('sonata.customer.manager')->save($customer);
        }

    }


}
