<?php
/**
 * Created by PhpStorm.
 * User: carvajal
 * Date: 11/10/16
 * Time: 10:37 PM
 */


namespace Application\Sonata\ProductBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

class VatRate{

    private $id;

    private $name;

    private  $rate;

    private  $deleted;

    private $vector_id;




    /**
     * Set name
     *
     * @param string $name
     *
     * @return VatRate
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set vectorId
     *
     * @param integer $vectorId
     *
     * @return VatRate
     */
    public function setVectorId($vectorId)
    {
        $this->vector_id = $vectorId;

        return $this;
    }

    /**
     * Get vectorId
     *
     * @return integer
     */
    public function getVectorId()
    {
        return $this->vector_id;
    }

    /**
     * Set rate
     *
     * @param integer $rate
     *
     * @return VatRate
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get rate
     *
     * @return integer
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return VatRate
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
