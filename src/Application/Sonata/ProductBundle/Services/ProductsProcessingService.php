<?php
/**
 * Created by PhpStorm.
 * User: carvajal
 * Date: 11/10/16
 * Time: 11:21 PM
 */
namespace Application\Sonata\ProductBundle\Services;


use Application\Sonata\ClassificationBundle\Entity\Category;
use Application\Sonata\ProductBundle\Entity\Wine;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Monolog\Logger;

use Sonata\ClassificationBundle\Document\CategoryManager;
use Sonata\ProductBundle\Entity\ProductCategoryManager;

class ProductsProcessingService{

    protected  $em;
    protected  $logger;
    private $productCategoryManager;
    private $ok_response_str = "<ns1:UpdateResponse><Status>OK</Status></ns1:UpdateResponse>";

    public function __construct(Logger $logger,
                                EntityManager $em,
                                ProductCategoryManager $productCategoryManager,
                                CategoryManager $categoryManager){
        $this->em  = $em;
        $this->logger = $logger;
        $this->productCategoryManager = $productCategoryManager;
        $this->categoryManager = $categoryManager;
    }

    public function process($productsXml){

        $wine_repository = $this->em->getRepository('ApplicationSonataProductBundle:Wine');





        foreach ($productsXml->product as $vector_product){

            $id = $vector_product->productid;
            $wine = $wine_repository->findOneBy(["vector_id"=> $id]);
            if($wine == null){
                $this->logger->info("Adding new product");
                $wine = new Wine();

            }

            $wine->setName($vector_product->name);
            $wine->setPrice(floatval($vector_product->Cost_net));

            $wine->setPriceIncludingVat(false);

            $wine->setVatId($vector_product->vatid);
            $wine->setVatRate(floatval($vector_product->Cost_VAT));
            $this->parseWineDescription($wine, $vector_product->description);
            $wine->setBarcode($vector_product->barcode);
            $wine->setSupplier($vector_product->supplier);
            $wine->setMinStockLevel(intval($vector_product->minstocklevel));
            $wine->setVolume(floatval($vector_product->volume));
            $wine->setSku($vector_product->sku);
            $wine->setVectorId($vector_product->productid);

            $this->productCategoryManager->addCategoryToProduct($wine, $this->categoryManager->findOneBy(["name"=>"WINES"]),true);
            $wine->setEnabled(true);

            $this->em->persist($wine);
        }



        $this->em->flush();

        return new \SoapVar($this->ok_response_str,XSD_ANYXML);
    }

    private function parseWineDescription(Wine $product, $desc_str){

        $values = explode("<br/>",$desc_str);

        $props = ["ORIGIN:","GRAPES:","STYLE:","FOOD:"];
        if (count($values) > 1){
            $this->logger->critical(print_r($values,true));

            foreach($values as $value){

                if(substr($value,0,strlen($props[0]) ) === $props[0]){
                    //origin
                    $start = strlen($props[0]) + 1;
                    $len = strlen($value) - $start + 1;
                    $origin = substr($value,$start,$len);

                    $product->setOrigin($origin);

                }
                else if(substr($value,0,strlen($props[1]) ) === $props[1]){
                    //grapes
                    $start = strlen($props[1]) + 1;
                    $len = strlen($value) - $start + 1;
                    $grapes = substr($value,$start,$len);
                    $product->setGrapes($grapes);

                }
                else if(substr($value,0,strlen($props[3]) ) === $props[3]){
                    //food
                    $start = strlen($props[3]);
                    $len = strlen($value) - $start ;
                    $food =  substr($value,$start,$len);


                    $product->setFood($food);

                }else if(substr($value,0,strlen($props[2]) ) === $props[2]){
                    //style
                    //TODO: process style
                }else{
                    $product->setRawDescription($value);
                    $product->setRawShortDescription($value);

                }
            }
        }else {
            $product->setRawDescription($desc_str);
            $product->setRawShortDescription($desc_str);

        }


    }

}