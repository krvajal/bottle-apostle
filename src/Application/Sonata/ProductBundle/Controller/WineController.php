<?php

/*
 * This file is part of the Sonata package.
 *
 * (c) Thomas Rabaix <thomas.rabaix@sonata-project.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Application\Sonata\ProductBundle\Controller;

use Sonata\ProductBundle\Controller\BaseProductController;


use Symfony\Component\Form\FormView;
use Sonata\Component\Basket\BasketElementInterface;
use  Sonata\Component\Basket\BasketInterface;

use Symfony\Component\HttpFoundation\Response;

/**
 * Overwrite methods from the BaseProductController if you want to change the behavior
 * for the current product
 *
 */
class WineController extends BaseProductController
{

     /**
     * @param FormView               $formView
     * @param BasketElementInterface $basketElement
     * @param BasketInterface        $basket
     *
     * @return Response
     */
    public function renderFormBasketElementAction(FormView $formView, BasketElementInterface $basketElement, BasketInterface $basket)
    {
        

        return $this->render("ApplicationSonataProductBundle:Wine:form_basket_element.html.twig",[
            'formView'=>$formView,
            'basketElement' => $basketElement,
            'basket' => $basket,
            'currency' => $this->get('sonata.price.currency.detector')->getCurrency()
            ]);
    }

    public function viewAction($product)
    {
        if (!is_object($product)) {
            throw new NotFoundHttpException('invalid product instance');
        }

        $provider = $this->get('sonata.product.pool')->getProvider($product);

        $formBuilder = $this->get('form.factory')->createNamedBuilder('add_basket', 'form', null, array('data_class' => $this->container->getParameter('sonata.basket.basket_element.class'), 'csrf_protection' => false));
        $provider->defineAddBasketForm($product, $formBuilder);

        $form = $formBuilder->getForm()->createView();

        $currency = $this->get('sonata.price.currency.detector')->getCurrency();

        // // TODO: Add twitter/FB metadata
        // $this->updateSeoMeta($product, $currency);
        return $this->render('SonataProductBundle:Wine:view.html.twig',[
            'product'=> $product,
            'provider'=> $provider,
            'currency' => $currency,
            'form' => $form
        ]);
        
    }

    /*
     * @Route("/api/v1/products/wines/"name="ba_product_wine_grid_element")
     */
    public function renderGridProductViewAction(){

        $product_id = $this->get('request')->get('product_id');

        $currency = $this->get('sonata.price.currency.detector')->getCurrency();
        $product = $this->get("sonata.product.set.manager")->find($product_id);


        return $this->render("ApplicationSonataProductBundle:Wine:product-box.html.twig",[
            'product'=>$product,
            'currency'=>$currency
        ]);

    }


}
