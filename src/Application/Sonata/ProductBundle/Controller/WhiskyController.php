<?php

/*
 * This file is part of the Sonata package.
 *
 * (c) Thomas Rabaix <thomas.rabaix@sonata-project.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Application\Sonata\ProductBundle\Controller;

use Sonata\Component\Basket\BasketElementInterface;
use Sonata\ProductBundle\Controller\BaseProductController;
use Symfony\Component\Form\FormView;
use Sonata\Component\Basket\BasketInterface;

/**
 * Overwrite methods from the BaseProductController if you want to change the behavior
 * for the current product
 *
 */
class WhiskyController extends BaseProductController
{


    public function renderFormBasketElementAction(FormView $formView, BasketElementInterface $basketElement, BasketInterface $basket)
    {


        return $this->render("ApplicationSonataProductBundle:Whisky:form_basket_element.html.twig",[
            'formView'=>$formView,
            'basketElement' => $basketElement,
            'basket' => $basket,
            'currency' => $this->get('sonata.price.currency.detector')->getCurrency()
        ]);
    }

}
