<?php
/**
 * Created by PhpStorm.
 * User: carvajal
 * Date: 11/14/16
 * Time: 4:53 PM
 */

namespace Application\Sonata\PaymentBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class StripeCardsController extends Controller {



    /**
     * @Route("/api/v1/stripe/cards",name="api_stripe_card_list")
     * Returns a JSON with the list of cards for the customer
     */


    public function listCardsAction(){



            $request = $this->get('request');
            $cus_id = $request->get("cus_id","");

        if(strlen($cus_id)> 0){
            \Stripe\Stripe::setApiKey("sk_test_fINM3JLxGWTnvAFEG1Gl2fHj");

            $stripeCustomer = \Stripe\Customer::retrieve( ["id"=>$cus_id,  "expand" => ["default_source"]] );
            $cards = $stripeCustomer->sources->all(array("object" => "card"));
            

            return new JsonResponse($cards->__toArray($recursive=true) );

        }


    }


     /**
     * @Route("/api/v1/stripe/customer",name="api_stripe_customer")
     */
    public function getCustomerData()
    {
        $request = $this->get('request');
        $cus_id = $request->get("cus_id","");

        if(strlen($cus_id)> 0){
            \Stripe\Stripe::setApiKey("sk_test_fINM3JLxGWTnvAFEG1Gl2fHj");

            $stripeCustomer = \Stripe\Customer::retrieve( ["id"=>$cus_id,  "expand" => ["sources"]] );



            return new JsonResponse($stripeCustomer->__toArray($recursive=true) );

        }
    }

    /**
     * @Route("/api/v1/stripe/customer/addcard",name="api_stripe_customer")
     */
    public function addCard()
    {
        $request = $this->get('request');
        $cus_id = $request->get("cus_id","");
        $card_id = $request->get("card_id","");
        if(strlen($cus_id)> 0 && strlen($card_id)){

            \Stripe\Stripe::setApiKey("sk_test_fINM3JLxGWTnvAFEG1Gl2fHj");

            $stripeCustomer = \Stripe\Customer::retrieve( ["id"=>$cus_id,  "expand" => ["sources"]] );
            $stripeCustomer->sources->create(array("source" => $card_id));

            return new JsonResponse($stripeCustomer->__toArray($recursive=true) );

        }

    }







}