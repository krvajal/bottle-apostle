<?php

namespace Application\Sonata\NewsBundle\Controller;


use AppBundle\Entity\ContactRequest;
use AppBundle\Form\ContactRequestType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;

class NewsController extends Controller
{
   
    public function showTagsAction()
    {
        
        $tagManager = $this->get('sonata.classification.manager.tag');
        $pager = $tagManager->getPager([],1,100);
        
               
        
        return $this->render('components/tag-list.html.twig',[
            'tags'=>$pager->getResults()
            ]);
    }


}