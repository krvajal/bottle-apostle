
BottleApostle.controller("MainProductsSidebarCtrl",function($scope, $http){


    // handle the price slider
    $scope.price_slider = {
        minValue: 0,
        maxValue: 500,
        options:{
            floor: 0,
            ceil: 500,
            step: 1,
            minRange: 120,
            pushRange: true,
            hideLimitLabels: true,
            translate: function(value) {
                return '<p style="color: black;">£' + value + '</p>';
            }
        },
    };

    $scope.contries_selector_open = false;
    $scope.selected_country  = "";


});