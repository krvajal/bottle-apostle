
/*
This controller handles the pagination of the products in the main catalog
It takes care of loading more products from the catalog async
and showing them
 */

BottleApostle.controller("CatalogSliderCtrl",function($scope, $element){

    $scope.page = 1;
    $scope.maxPerPage = 2; // this changes depending on the layout
    $scope.categoryId = null;

    var route = Routing.generate("ba_products_category_provider");
    var $productsContainer = angular.element($element).find(".horz_prod_slider");
    var $next_button =  angular.element($element).find(".grid-navigator-right");

    console.log({$productsContainer});
    /* handler for the next button*/
    $scope.next = function () {


        console.log("Loading more products");
        console.log( "category",$scope.categoryId );
        console.log({route});

        //call the sever to get the html for the new products


        $.get(route,{'page':($scope.page + 1),'category_id':$scope.categoryId}, function(data){

            $scope.page = $scope.page  + 1;

            //TODO: check for errors here
            $productsContainer.html(data);
            $productsContainer = angular.element($element).find(".horz_prod_slider");

            //as the button was destroyed, the previous one does not exist anymore
            //so we have to locate the new one
            $next_button =  angular.element($element).find(".grid-navigator-right");
            //now bind the click event to the button
            $next_button.click($scope.next);
            //load the plus and minus spinners
            loadSpinners();

        });

    }


});