

var parseAddressString = function (stringaddress, clean=false){


    var address = {};

    address.stringaddress = stringaddress;

    stringaddress = stringaddress.split(",");


    address.line1 = stringaddress[0];
    address.line2 = stringaddress[1];
    address.line3 = stringaddress[2];
    address.line4 = stringaddress[3];
    address.locality = stringaddress[5];
    address.town = stringaddress[6];
    address.county =  stringaddress[7];

    if(clean === true) {
        var cleaup = function () {
            var formattedaddress = "";
            for (var i = 0; i < 7; i++) {

                if (stringaddress[i].trim().length) {
                    if (i)
                        formattedaddress += ", ";
                    formattedaddress += stringaddress[i].trim();
                }
            }
            return formattedaddress;
        };
        address.str = cleaup(stringaddress);
    }

    return address;
};

var saveAddressToBasket = function(url, postcode, address, callback){


    console.log('saving delivery address');
    console.log(url);
    var ajax =  $.ajax(url,{
        method: 'POST',
        data: {
            address1: address.line1,
            address2: address.line2,
            address3: address.line3,
            postcode: postcode,
            city: address.town,
            country: 'GB'
        },

    });
    ajax.done(function (data) {
        callback(data);
    });
    ajax.fail(function (data) {
        data.success= false
        callback(data);
    })
};



BottleApostle.controller("CheckoutSidebarCtrl",["$scope","$http", "$q","baCheckoutService" ,function($scope, $http, $q, checkoutService){


    let stripe_publishable_key = "pk_test_TwOpkCMqy1pYqkLeQjpSedbr";

    var get_address_api_key = "ghJc9CTa_0yCQL8QfJllzA5375";

    $scope.postcodeAutocomplete = function(str, timeoutPromise){
        console.log("Searching...");
        return $http.get( "https://api.postcodes.io/postcodes/" + str + "/autocomplete", {q: str}, {timeout: timeoutPromise});
    };

    $scope.months = [
        "JAN",
        "FEB",
        "MAR",
        "APR",
        "MAY",
        "JUN",
        "JUL",
        "AUG",
        "SEP",
        "OCT",
        "NOV",
        "DIC"
    ];

    $scope.years = [
        2016,
        2017,
        2018,
        2019,
        2020
    ];

    $scope.billing_address_list = [];
    $scope.delivery_address_list= [];

    $scope.lookupBillingAddress=function(){

        console.log($scope.basket.billing_address);
        var postcode = $scope.basket.billing_address.postcode.title;


        $http.get("https://api.getAddress.io/v2/uk/" + postcode + "?api-key=" + get_address_api_key).success(function (data, status, headers, config) {
            console.log("Address list");
            console.log(data);

            $scope.billing_address_list = data.Addresses;

            //select first by default
            $scope.basket.billing_address.str  = data.Addresses[0];

        });


    };

    $scope.lookupDeliveryAddress = function(){

        console.log($scope.basket.delivery_address);
        var postcode = $scope.basket.delivery_address.postcode.title;


        $http.get("https://api.getAddress.io/v2/uk/" + postcode + "?api-key=" + get_address_api_key).success(function (data, status, headers, config) {
            console.log("Address list");
            console.log(data);

            $scope.delivery_address_list = data.Addresses;

            //select first by default
            $scope.basket.delivery_address.str  = data.Addresses[0];

        });


    };




    $scope.postcodeFormatter = function (data) {
        console.log("results");
        console.log(data);
        if(data.result === undefined){

            return null;
        }
        for(var i = 0; i < data.result.length; i++){
            var value = data.result[i];
            data.result[i] ={title: value};

        }
        console.log(data);

        return data;
    };

    $scope.setBillingAddress = function(){


    };



    $scope.setDeliveryAddress = function (){


    };

    //the checkout_step is separated into 2 steps
    // step 1: collect addresses
    // step 2: collect payment method
    $scope.checkout_step  = 1;

    $scope.PAYMENT_METHODS = ["stripe","paypal"];

    $scope.basket = {
        delivery_address: {},
        billing_address: {},
        delivery_same: false,
        payment_method: {code:"stripe"}, //default one
        gift: false,
        gift_instructions: null,
        has_delivery_instructions: false,
        delivery_instructions: null,
        customer_id: null
    };

    $scope.generate_stripe_token = function(){

        var stripe_response_handler  = function(status,response){

            console.log(response);

            if (response.error) { // Problem!

                // Show the errors on the form:
                var error =  $('#checkout-card-form').find('.payment-errors');
                error.text(response.error.message);
                error.show();
//            $form.find('.submit').prop('disabled', false); // Re-enable submission

            } else { // Token was created!

                // Get the token ID:
                var token = response.id;

                $scope.basket.stripe_token = token;

                var $form = $('#checkout-card-form');
                // Insert the token ID into the form so it gets submitted to the server:
                $form.append($('<input type="hidden" name="stripeToken">').val(token));
                console.log("sending card to server");
                // Submit the form:
                $form.ajaxSubmit(function (data) {
                    console.log(data);
                    if(data.success){
                        makePayment();

                    }
                    else{
                        alert("failed to add credit card");
                    }
                });
            }
        };


        Stripe.card.createToken($form, stripe_response_handler);
    }


    $scope.NextStep = function () {

        $scope.error = false;
        //check billing address
        if($scope.basket.billing_address.postcode.title  === undefined){

            $scope.error = true;
            $scope.error_message = "Billing postcode can't be empty";
            return;
        }
        if($scope.basket.billing_address.str === undefined){

            $scope.error = true;
            $scope.error_message = "Billing address can't be empty";
            return;
        }
        if($scope.basket.delivery_same){

            $scope.basket.delivery_address = $scope.basket.billing_address;

        }
        if($scope.basket.delivery_address.postcode.title  === undefined){

            $scope.error = true;
            $scope.error_message = "Delivery postcode can't be empty";
            return;
        }
        if($scope.basket.delivery_address.str === undefined){

            $scope.error = true;
            $scope.error_message = "Delivery address can't be empty";
            return;
        }

        var address = parseAddressString($scope.basket.delivery_address.str);


        checkoutService.saveDeliveryAddress( $scope.basket.delivery_address.postcode.title, address)
            .then(function(payload){

                //delivery address here

                //payload: data, status, headers, config
                // what I need is the data member

                console.log(payload);
                if(payload.data.success == true){
                    alert("saved delivery address");


                    return checkoutService.saveBillingAddress(
                        $scope.basket.billing_address.postcode.title,
                        parseAddressString( $scope.basket.billing_address.str)
                        );

                }else{
                    //failed to save billing address
                    $q.defer()
                }

            },function(httpError){
                q.defer();

            }).then(function(payload) {
            //billing address
            console.log("saved billing address");
            alert("saved billing address");

            return $http.post(Routing.generate("sonata_basket_delivery_method_rest"),
                {"delivery_method_code": "take_away"});

            },function(httpError){
                    // error in billing address
               $q.defer();
            })
            .then(function(payload){
                //got delivery method specified
                alert("Setted delivery method");

                var postcode = $scope.basket.billing_address.postcode
                $scope.basket.billing_address = parseAddressString($scope.basket.billing_address.str, true);
                $scope.basket.billing_address.postcode = postcode;
                postcode = $scope.basket.delivery_address.postcode;
                $scope.basket.delivery_address = parseAddressString($scope.basket.delivery_address.str,true);
                $scope.basket.delivery_address.postcode = postcode;

                $scope.checkout_step  = 2;

            },function(){
                //last error one

            });


    }


    $scope.payment = {card_number:null, card_exp_month:null, card_exp_year: null, card_cvc: null,card_holder_name: null};

    //check the card for erros and update ui,
    //return true if card is valid
    $scope.validateCard = function(){


        if(!Stripe.card.validateCardNumber($scope.payment.card_number)){

            $scope.error = true;
            $scope.error_message = "Invalid card number";
            return false;
        }
        if(!Stripe.card.validateExpiry($scope.payment.card_exp_month,$scope.payment.card_exp_year)){

            $scope.error_message = "Invalid card expiration date";
            $scope.error  = true;
            return false;
        }

        if(!Stripe.card.validateCVC($scope.payment.card_cvc)){
            $scope.error_message = "Invalid card CVC/CVV number";
            $scope.error  = true;
            return false;
        }
        $scope.error = false;
        return true;

    };


    $scope.CompleteTransaction = function(){


        var address =  parseAddressString($scope.basket.billing_address.str);
        console.log($scope.payment);

        Stripe.setPublishableKey(stripe_publishable_key);

        if($scope.validateCard()) {
            console.log("Generating stripe token");

            Stripe.card.createToken({
                number: $scope.payment.card_number,
                cvc: $scope.payment.card_cvc,
                exp_month: $scope.payment.card_exp_month,
                exp_year: $scope.payment.card_exp_year,
                name: $scope.payment.card_holder_name,
                address_zip: $scope.basket.billing_address.postcode.title,
                address_line1: address.line1,
                address_line1: address.line2,
                address_country: address.country
            }, function (status, response) {

                if (response.error) {
                    //error here
                    alert(response.error.message);

                } else {

                    var token = response.id;
                    $scope.basket.payment_method.stripe_token = token;
                    alert("generated stripe token");

                    $http.post(Routing.generate("sonata_basket_payment_rest"), {
                        payment_method: $scope.basket.payment_method.code,
                        stripe_token: $scope.basket.payment_method.stripe_token
                    }).then(function(payload){

                        alert("added payment method");
                        //submit the form, vannilla javascript here
                        document.getElementById('make-payment-form').submit()


                    });
                }
            });
        }


    }



}]);

BottleApostle.factory("baCheckoutService",function($http){


    var formatAddressData=function(postcode,address){

        var  data = {
            address1: address.line1,
            address2: address.line2,
            address3: address.line3,
            postcode: postcode,
            city: address.town,
            country: 'GB'
        };
        return data;
    };

    return {
        saveBillingAddress: function(postcode, address){
            var data = formatAddressData(postcode,address);
            console.log("Saving billing address");
            console.log(data);

            return $http.post(
                Routing.generate("sonata_basket_payment_address_rest"),
                formatAddressData(postcode,address));
        },
        saveDeliveryAddress: function (postcode, address) {

            var data = formatAddressData(postcode,address);
            console.log("Saving delivery address");
            console.log(data);
            return $http.post(
                Routing.generate("sonata_basket_delivery_address_rest"),
                formatAddressData(postcode,address));
        }

    };

});


