
BottleApostle.controller("WinesAdvancedSearchCtrl",function($scope, $http){

    $scope.advanced_search  = false;
    $scope.show_styles = false;
    $scope.show_types = false;
    $scope.show_countries = false;
    $scope.show_sizes = false;
    $scope.show_closure = false;
    $scope.show_methods = false;

    $scope.countries = [{code:"SP", name:"Spain"},{code:"IT", name:"Italy"} ];
    $scope.types = ["CAVERNET","PINOT NOIR","BORDEAUX"];
    $scope.closures = ["CORK","SCREW CAP", "Synthetic Cork","Vinolok","Zork"];
    $scope.methods = ["saignée","method2","method23"];

    $scope.selected_country = null;
    $scope.selected_type =  null;

    $http.get("/products/api/wine_styles").then(function (response) {
        $scope.styles = response.data;

    });


    $scope.selectCountry = function(count){
        $scope.selected_country = count;
        $scope.show_countries  = false;
    }
    $scope.selectType = function(type){
        $scope.selected_type = type;
        $scope.show_types  = false;
    }
    $scope.selectClosure = function(closure){
        $scope.selected_closure = closure;
        $scope.show_closure  = false;
    }
    $scope.selectStyle = function(style){
        $scope.selected_style = style;
        $scope.show_styles  = false;
    }
    $scope.selectMethod = function(method){
        $scope.selected_method = method;
        $scope.show_methods  = false;
    }

    $scope.filterWines = function(){


    }

    $scope.slider = {
        minValue: 0,
        maxValue: 500,

        options:{
            floor: 0,
            ceil: 500,
            step: 1,
            hideLimitLabels: true,
            minRange: 120,
            pushRange: true,
            translate: function(value) {
                return '<p style="color: black;">£' + value + '</p>';
            }
        }
    }


});