$.widget( "ba.crave", {
 
    options: {
        maxbottles: 6,
    },
 
    _create: function() {
        
        // this.element.addClass('crate');
        this.element.submit(function(e){
                var showResponse = function(responseText, statusText, xhr, $form){

                    //global function in basket.js
                    // call the backedn and the basket gets updated
                    updateBasket();
                    alert("Product added");

                 };
                var options = {
                    success: showResponse
                };
                $(this).ajaxSubmit(options);
                return false;
        });
        var cont = $( "<div></div>" )
        .appendTo( this.element.find('.crates')).cratescontainer();
        var counter =  this.element.find('.shopping-spinner').spinner({
        icons: { down: "ui-icon-minus", up: "ui-icon-plus" },
        min: 1
        });
        this.element.find('.add-crate-btn').click(function(){
            cont.cratescontainer('addcrate');
            var val = counter.spinner('value');
            counter.spinner('value', val + 6);

            // cont.cratescontainer('addbottle');
        });

        var total = this.element.find('.total-label');
        var formatter = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'GBP',
            minimumFractionDigits: 2,
            });
        counter.on('spinstop',function(e,ui){

            var value = $(this).spinner('value');
            cont.cratescontainer('bottles', value);

           
            total.text(formatter.format(unitprice * value));
            console.log(value);
        });

        var unitprice = parseFloat(this.element.find('input[name="product_unit_price"]').val());
        
        total.text(formatter.format(unitprice));
        console.log(unitprice);
        this._elements.cont = cont;
        this._elements.counter = counter;
        this._elements.unitprice = unitprice;
        this._elements.total = total;
        
    },
    _elements : {}
});