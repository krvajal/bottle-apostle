$.widget( "ba.winecrate", {
 
    options: {
        bottles: 1,
        maxbottles: 6
    },
 
    _create: function() {
        
        this.element.addClass('crate');
       this._updateBottles();
        
        
    },
 
    // Create a public method.
    bottles: function( value ) {
 
        // No value passed, act as a getter.
        if ( value === undefined ) {
            return this.options.bottles;
        }
 
        // Value passed, act as a setter.
        this.options.bottles = this._constrain( value );
        
        this._updateBottles();
        
    },
 
    // Create a private method.
    _constrain: function( value ) {
        if ( value > this.options.maxbottles ) {
            value = this.options.maxbottles;
        }
        if ( value < 0 ) {
            value = 0;
        }
        return value;
    },
    _updateBottles : function(){
        this.element.empty();
        var bottles = this.options.bottles;
        var maxbottles = this.options.maxbottles;

        for(var i = 0; i < maxbottles; i++){
            if(i < bottles){
                var bottle = $('<div class="crate-bottle-selected"></div>');
                this.element.append(bottle);
            }else{
                var bottle = $('<div class="crate-bottle"></div>');
                this.element.append(bottle);
            }
        }

    }

});
