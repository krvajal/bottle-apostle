 $.widget( "ba.cratescontainer", {
 
    options: {
        crates: [{bottles:1}]
    },
 
    _create: function() {
        
        this.element.addClass('crates-container').addClass('flex1');
        this._updateCrates();
    },
 
    // Create a public method.
    crates: function( value ) {
 
        // No value passed, act as a getter.
        if ( value === undefined ) {
            return this.options.crates;
        }
 
        // Value passed, act as a setter.
        this.options.bottles = this._constrain( value );
        
        this._updateCrates();
        
    },
    addcrate: function(){
        var crate = {bottles: 6};
        this.options.crates.push(crate);
        this._updateCrates();

    },
    addbottle:function(){
        // got trough the crates and fill one bottle
        var crates = this.options.crates;
        var added = false;
        for(var i = 0; i < crates.length; i++){
          var maxbottles = 6;
          console.log('crate ', i ,': ', crates[i].bottles);
          if(crates[i].bottles < maxbottles){
              this.options.crates[i].bottles++;
              added = true;
              break;
          }
        }
        if(!added){
          this.options.crates.push({bottles:1});
        }
        this._updateCrates();
        //no crate to add bottle
        // ask what to do

    },
    bottles: function(value){

        // No value passed, act as a getter.
        if ( value === undefined ) {
          var bottles = 0;
            for(var  i = 0; i < this.options.crates.length; i++){
               bottles = bottles + this.options.crates[i].bottles;
            }
        }
        //setter
        this.options.crates = [];
        var fullcrates = parseInt(value /6);
        console.log('full crates: ', fullcrates );
        var bottles = value % 6;
        console.log('bottles: ', bottles);
        for(var i = 0; i < fullcrates; i++){
           this.addcrate();
           
        }  
        for(var i = 0; i  < bottles; i++){
          this.addbottle();
        }


    },


    // Create a private method.
    _constrain: function( value ) {
       
        if ( value < 1 ) {
            value = 1;
        }
        return value;
    },
    _updateCrates : function(){
        this.element.empty();
        var crates = this.options.crates;
        for(var i = 0; i < crates.length ; i++){
            var bar = $( "<div></div>" )
            .appendTo( this.element ).winecrate({
                bottles : crates[i].bottles
            });

        }
    }

});