

function  loadSpinners() {

    $('.shopping-spinner').spinner({
        icons: {down: "ui-icon-minus", up: "ui-icon-plus"},
        min: 1

    });
}

$(document).ready(function(){


    loadSpinners();


    
});


var BottleApostle = angular.module("BottleApostleApp",["angucomplete-alt",'ui.select', 'ngSanitize', "rzModule","ngAnimate"]);

BottleApostle.config(function($interpolateProvider){

    $interpolateProvider.startSymbol('[[').endSymbol(']]');
});



