$(document).ready(function(){


    $('#connect-btn').click(function (event) {
        console.log('clicked');

        $('#connect-dropdown').slideToggle();
    });

    function login(url,csrf_token, username, password){

        console.log("Performing logging for user ", username);
        console.log("Loggin url ", url);
        /* Send the data using post */
        var posting = $.post(url, {
            '_csrf_token': csrf_token,
            '_username': username,
            '_password': password

        });

        posting.fail(function(xhr, status, error) {
            console.log("failed");
            console.log(error);
        });
        /* Put the results in a div */
        posting.done(function(data) {
            console.log("data back:",data);
            console.log(data.success);
            if(data.success === true){
                location.reload(true);
            }else{


            }
        });

    }

    /* attach a submit handler to the form */
    $("#login-form").submit(function(event) {
        /* stop form from submitting normally */
        event.preventDefault();
        console.log('got it');
        /* get some values from elements on the page: */
        var $form = $(this),
            _csrf_token = $form.find('input[name="_csrf_token"]').val(),
            _username = $form.find('input[name="_username"]').val(),
            _password = $form.find('input[name="_password"]').val(),

            url = $form.attr('action');

            login(url, _csrf_token, _username, _password);

    });



    /* attach a submit handler to the form */
    $("#register-form").submit(function(event) {
        /* stop form from submitting normally */
        event.preventDefault();

        /* get some values from elements on the page: */
        var $form = $(this),
            firstName = $form.find('input[name="app_user_registration[firstName]"]').val(),
            lastName = $form.find('input[name="app_user_registration[lastName]"]').val(),
            email = $form.find('input[name="app_user_registration[email]"]').val(),
            password = $form.find('input[name="app_user_registration[plainPassword][first]"]').val(),
            passwordConfirmation = $form.find('input[name="app_user_registration[plainPassword][second]"]').val(),
            subscribed = $form.find('input[name="app_user_registration_suscribedToNewsletter"]').val(),
            token = $form.find('#app_user_registration__token').val(),
            url = $form.attr('action');

        console.log(token);
        /* Send the data using post */
        var posting = $.post(url, {
            'app_user_registration[firstName]': firstName,
            'app_user_registration[lastName]': lastName,
            'app_user_registration[email]':email,
            'app_user_registration[plainPassword][first]':password,
            'app_user_registration[plainPassword][second]':passwordConfirmation,
            'app_user_registration_suscribedToNewsletter':subscribed,
            'app_user_registration[_token]': token
        });
        posting.fail(function(xhr, status, error) {
           console.log("failed");
            console.log(error);
        });
        /* Put the results in a div */
        posting.done(function(data) {
            console.log("register data: ", data);
            if(data.success === true){
                login( $("#login-form").attr('action'), $("#login_csrf_token").val(), email, password);
            }else{
                //TODO: registration failed, show errors
                console.log('registration failed');

            }
        });
    });









});
