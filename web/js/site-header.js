/**
 * Created by Mercy on 7/28/2016.
 */
$(document).ready(function(){

    $("#events-menu").mouseenter(function () {
        $("#events-dropdown").slideDown();
    });

    $("#events-menu").mouseleave(function () {
        $("#events-dropdown").slideUp();
    });

    $("#about-menu").mouseenter(function () {
        $("#about-dropdown").slideDown();
    });

    $("#about-menu").mouseleave(function () {
        $("#about-dropdown").slideUp()  ;
    });

    setTimeout(function(){

        $('#cart-dropdown').css('opacity',1.0);
        $('#cart-dropdown').css('display','none');


    }, 10);

    $("#cart-btn ").parent().mouseenter(function () {
        
        $("#cart-dropdown").slideDown('slow');

    });
    $("#cart-btn").parent().mouseleave(function () {
       if($('#cart-dropdown').is(':hover')){
           //noop
       } else{
           $('#cart-dropdown').slideUp('fast');
       }
    });
    // $("#cart-dropdown").mouseleave(function () {
    //     if($('#cart-btn').is(':hover')){
    //         //noop
    //     } else{
    //         $('#cart-dropdown').slideUp('fast');
    //     }
    // });


    // $("#products-menu").mouseenter(function () {
    //     $("#products-dropdown").css("display", "flex")
    //         .hide()
    //         .fadeIn();

    // });

    // $("#products-menu").mouseleave(function () {
    //     $("#products-dropdown").fadeOut();

    // });

    
});