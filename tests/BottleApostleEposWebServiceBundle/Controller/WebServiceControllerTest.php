<?php

namespace BottleApostle\RestApiBundle\Tests\Controller;

use SimpleXMLElement;
use SoapParam;
use SoapVar;
use stdClass;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

use SoapClient;
use Symfony\Component\Config\Definition\Exception\Exception;

class Update
{

    private $updateXML;

    function Update($updateXml)
    {
        $this->updateXML = $updateXml;


    }
}

class WebServiceControllerTest extends WebTestCase
{
    public function testIndex()
    {

        $authorID = 12323;
        $userID = 1;
        $userPassword = 10;
        $customerEmail = "a";
        $xmlr = new SimpleXMLElement("<CustomerSearch></CustomerSearch>");
        $xmlr->addChild('AuthorID', $authorID);
        $xmlr->addChild('UserID', $userID);
        $xmlr->addChild('UserPassword', $userPassword);
        $xmlr->addChild('Email', $customerEmail);

        $params = new stdClass();
        $params->xml = $xmlr->asXML();


        $exceptions = array();
        $client = new SoapClient("http://127.0.0.1:8000/epos/soap?wsdl", array('trace' => 1, 'exceptions' => $exceptions));
        var_dump($client->__getFunctions());
        var_dump($client->__getTypes());

        $struct = new Update($xmlr->asXML());
        $soapstruct = new SoapVar($struct, SOAP_ENC_OBJECT, "Update", "http://soapinterop.org/xsd");
        try {
            $response = $client->Update(array('Update' => $xmlr->asXML()));
            var_dump($client->__getLastRequest());
        } catch (Exception $e) {
            echo $e->getMessage();
        }


    }


}